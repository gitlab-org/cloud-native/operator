---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Database Migrations

The `DatabaseMigration` upgrades the database of the GitLab Rails application.

The controller of the `DatabaseMigration` custom resource is responsible for:

* Generating the initial root password, if it was not specified.
* Generating a minimal rails configuration required to run the database migrations.
* Deploying a Job that runs the migrations.
* Setting a status condition to indicate if the migration is complete.

The controller is **not** responsible for:

* Ensuring that all database clients are compatible with the new schema version.
* Finalizing background migrations or checking the status of background migrations.

## Installation

The custom resource is usually created and managed by the `Instance`
controller. Do not create a `DatabaseMigration` for your GitLab instance
manually, unless you know what you are doing.

You can create your own `DatabaseMigration` custom resource by adapting
the [sample resource](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/config/samples/v2alpha2_databasemigration.yaml).
For all available options check the [`DatabaseMigration` API reference](../generated/api-docs.md#databasemigration).
