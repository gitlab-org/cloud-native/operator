---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Kubernetes Agent Server

The `KubernetesAgentServer` custom resource allows to deploy and configure
[GitLab agent server (KAS)](https://docs.gitlab.com/ee/administration/clusters/kas.html).

The controller of the `KubernetesAgentServer` custom resource is responsible for:

* Generating the required KAS secrets.
* Rendering the KAS configuration, workload and service.

## Installation

Create a `KubernetesAgentServer` custom resource by adapting the [sample resource](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/main/config/samples/v2alpha2_kubernetesagentserver.yaml)
with the URLs and Redis references of your installation.

For all available options check the [`KubernetesAgentServer` API reference](../generated/api-docs.md#kubernetesagentserver).

## Upgrading

The `KubernetesAgentServer` can be upgraded by either:

1. Setting the `gitlab.com/instance` instance label. The `Instance` controller will
   manage the version field and KAS upgrades for you.
1. Manually updating `spec.version`.

## Scaling

The `KubernetesAgentServer` custom resource implements a [scale subresource](https://kubernetes.io/docs/tasks/extend-kubernetes/custom-resources/custom-resource-definitions/#scale-subresource),
which means you can scale it using [`kubectl scale`](https://kubernetes.io/docs/reference/kubectl/generated/kubectl_scale/):

```bash
kubectl scale --replicas 3 kas/your-kas-resource
```

## Monitoring

You can integrate `KubernetesAgentServer` with your monitoring by scraping metrics
from the managed KAS `Service` at port 8151 (port name: `http-metrics`).

The Service reference is part of the KAS status (`status.sharedObjects`).
