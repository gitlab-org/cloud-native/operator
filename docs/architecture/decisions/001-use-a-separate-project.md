# Use a separate project and repository

**Date**: 2024-04-18

**Status**: Accepted

## Context

The existing [GitLab Operator](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator),
also known as "Chart-based GitLab Operator" and "GitLab Operator V1", is still
under development. It is [intended for limited production use](https://gitlab.com/groups/gitlab-org/cloud-native/-/epics/68),
and is expected to receive regular bug fixes and feature maintenance. It follows
the usual GitLab release cadence, and is published to publicly accessible
catalogs, such as OperatorHub and RedHat Operators.

At the same time we want to implement new features and experiment new ideas and
tools for the new GitLab Operator, also know as "GitLab Operator V2". Keeping
the two in the same repository poses various challenges, including maintaining
the dependencies and upholding the legacy scaffolding of the existing GitLab
Operator.

Also, it is unlikely that we support the automatic migration of the existing
GitLab custom resources, i.e. version `v1beta1`, that are managed by the GitLab
Operator V1 to the newer versions of custom resources that are managed by GitLab
Operator V2.

## Decision

We will move the development of the new GitLab Operator to a separate project
and repository. This project has its own processes, pipelines, and lifecycle,
and does not impact the existing GitLab Operator. It may use part of the CI
infrastructure of the existing GitLab Operator but ensures that its usage is
isolated, for example with different namespaces.

## Consequences

- It allows us to start fresh with updated tools, dependencies, and scaffolding
  that are better aligned with contemporary best practices.
- We have a more flexible playground to work on various ideas and contradictory
  changes without disrupting the release of the existing GitLab Operator.
- We need to set up a new project from scratch. This requires additional effort
  but at the same time allows us to tidy up, fix, and improve based on the
  lessons that we learned from the existing GitLab Operator and other projects.
- We are adding another active project that may require additional housekeeping
  or regular maintenance effort, for example for upgrading Go version or fixing
  CVEs.
- Once established, it will be very difficult to merge the two Operators at any
  point in time.
