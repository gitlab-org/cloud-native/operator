# API Development

## Decomposition

This approach offers several compelling advantages, including improved
comprehensibility, simplified implementation and testing processes, and enhanced
maintainability through distributed team ownership.

However, it's important to acknowledge the challenges that come with this
strategy. The modular monolithic nature of the GitLab Rails application presents
significant obstacles to decomposition into separate API resources. Furthermore,
the prevalent bidirectional relationships between GitLab components introduce
complexities that can lead to redundancy and complicate the decomposition
process.

Despite these challenges, the potential benefits make this an avenue worth
exploring for improving Kubernetes API of GitLab.

### Design forces

#### GitLab Rails is a modular monolith

GitLab Rails is a modular monolith that runs in different profiles, for example
as an application server in Puma, or job processor in Sidekiq. However, these
profiles are not isolated and share the same configuration model. This means
that the application configuration is a shared data structure that is available
to all modules of the Rails monolith. We cannot make any assumption about which
module uses which configuration items.

#### GitLab components have bidirectional relationships

It is not uncommon for GitLab components to have bidirectional relationships.
Most notably between GitLab Rails modules and other GitLab components such as
container registry. While application configuration contains references to these
"auxiliary" components, their configuration also depends on GitLab Rails
application.

For example container registry requires the JWT authentication provider endpoint
of application server (Puma) and may use its object store configuration. At the
same time GitLab Rails configuration reference container registry API and shares
its secret tokens. This is a bidirectional dependency and makes separation of
application server and container registry APIs difficult.

#### Necessity of an orchestrating resource

The scope of some instance-wide configuration and workflows can span beyond
individual components and involve multiple decomposed resources. For example,
GitLab version and edition, or upgrade workflow requires multiple resources to
interact with each other. This requires an orchestrating resource that all other
resources are associated with and get their instance-level configuration from
it.

### Design patterns

The main issue in API decomposition is identifying the boundary of components
and establishing the relationship between them, esp. when it comes to shared
configuration. The relationship between application server and container
registry is a good example.

In order to address this issue, we can use a _combination_ of three patterns.
The choice of the right patterns depend on type of the resources and the nature
of their relationship.

#### Explicit referencing

In explicit referencing, resource `A` has a direct reference to resource `B`,
which in Kubernetes nomenclature is known as `bRef` and usually is the qualified
name of resource `B`. This reference can contain more details such as namespace,
kind and API version of the referenced resource.

With this reference, the controller of `A` can access `B` and retrieve its
managed resources, including its configuration and use it. Ideally, the status
sub-resource of `B` should contain details of its sharable managed resources but
this is not a mandate. What is important is that `A`'s controller knows how to
locate, access, and consume `B`'s configuration.

In this sense, this is a strong relationship and tightly couples `A` to `B`. It
is useful when a significant portion of the configuration of `A` depends on `B`,
for example in Rails-based processes.

#### Implicit referencing

In implicit referencing, resource `A` does not have any direct reference to
resource `B`. Instead, the controller of `A` knows how to locate `B` with the
information that is only available in `A`, for example labels and annotations.
This is a common practice for many Kubernetes resources.

As opposed to explicit referencing, this type of referencing creates a looser
relationship between `A` and `B`. It is most useful when `A` does not know the
exact `B` instance at the time of creation, for example `B` is not created yet.

It is also useful for defining the return link of bidirectional relationships.
When `A` and `B` have a bidirectional relationship, the stronger link is defined
with an explicit reference, for example `A` references `B` with `bRef`, and the
weaker relationship is defined with an implicit reference, for example `B` has
label `instance: example` and uses the same label selector to find `A`, given
that both have the same label.

```mermaid
classDiagram
  class A {
    labels [instance = example]
    bRef
  }
  class B {
    labels [instance = example]
  }

  A --> B : bRef
  B ..> A : A{instance = example}
```

#### Redundant configuration

This may look like an anti-pattern, and if not applied to the right problem, it
certainly is. But in some settings it helps with decoupling of components and
offers more flexible configuration at the cost of redundancy.

If `A` and `B` share a small portion of their configuration and we want to keep
them independent from each other, duplicating the configuration in both of them
is the only viable choice. Since we want to keep the redundancy to the minimum,
the size of the shared configuration becomes a determinant factor in
applicability of this pattern.

### Design patterns in action

#### GitLab application server is a central resource

In the decomposed API, the GitLab application server, is the focal point of the
model. It is the owner of any configuration that Rails-based components consume.
All other Rails-based components, including job processor, database migrations,
even backup and restore, depend on it and through it they get access to the
underlying Rails configuration.

This is realized by explicitly referencing the application server. At the same
time the controller of application server may require information about other
components, for example the state of job processor during upgrade. This can be
realized with an implicit relationship.

```mermaid
classDiagram
  class ConfigMap {
    - gitlab.yaml
    - database.yaml
    - redis.yaml
  }
  class Secret {
    - secrets.yaml
  }

  class JobProcessor{
    appServerRef
  }
  class DatabaseMigration {
    appServerRef
  }

  DatabaseMigration --> ApplicationServer : appServerRef
  JobProcessor --> ApplicationServer : appServerRef

  ApplicationServer ..> JobProcessor : JobProcessor{instance}

  ApplicationServer --* ConfigMap : owns
  ApplicationServer --* Secret : owns
```

#### Some GitLab components can be independent

While the GitLab Rails-based components are tightly coupled, it is possible to
decouple some of the other components from them. Let's use container registry
as an example. As pointed out before, the container registry needs to know the
JWT authentication provider endpoint. It can also use the object store
configuration of GitLab Rails, esp. when it is in consolidated form. But this
unnecessarily couples these two components.

This is where we can leverage redundant configuration for one way of their
relationship by introducing fields to specify:

- URL or a reference to a Service resource for an application server. This can
  be even point to an external application server.
- References to any Secrets that may be required to communicate with application
  server.
- Details of object store provider, such as authentication and bucket name.

The application server also depends on container registry and it needs to know:

- Container registry Service endpoint.
- Some of its shared Secrets.

To realize this relationship, we can use explicit referencing. So an application
server directly references the container registry that it wants to use.

```yaml
---
apiVersion: gitlab.com/v2alpha2
kind: ContainerRegistry
metadata:
  name: example-registry
  labels:
    apps.kubernetes.io/instance: example
spec:
  authEndpoint:
    service: example-app-server-service
  objectStore:
    aws:

  ...
---
apiVersion: gitlab.com/v2alpha2
kind: ApplicationServer
metadata:
  name: example-app-server
  labels:
    apps.kubernetes.io/instance: example
spec:
  containerRegistryRef:
    name: example-registry
```

#### Bidirectional relationships can cause a deadlock

In the example above, to reconcile `example-registry`, the `ContainerRegistry`
controller depends on the availability of `example-app-server-service` Service.
But this resource will not be created until `ApplicationServer` controller
finishes with `example-app-server`.

At the same time, for reconciling `example-app-server`, `ApplicationServer`
controller needs its shared Secrets and Service, which will not be created until
`ContainerRegistry` controller finishes with `example-registry`.

There is a way to break out of this situation. If `ApplicationServer` and
`ContainerRegistry` controllers:

1. Create Secrets and Services before anything else.
1. Wait for the availability of Secrets and Services from the other party to
   dereference it. Note that at this stage the Service does not need to be
   backed by Kubernetes workload. Only the reference will suffice.
1. Configure the component.
1. Rollout the workload.

So as a general rule, each controller must:

1. Create the managed resources as soon as possible, esp. when they don't have
   any dependency.
1. Handle managed resources dependencies loosely and wait for their availability
   only when they are needed.
