[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gitlab-org/cloud-native/operator "Go Report Card")](https://goreportcard.com/report/gitlab.com/gitlab-org/cloud-native/operator)
[![Go Reference](https://pkg.go.dev/badge/gitlab.com/gitlab-org/cloud-native/operator.svg)](https://pkg.go.dev/gitlab.com/gitlab-org/cloud-native/operator)

[![Coverage](https://gitlab.com/gitlab-org/cloud-native/operator/badges/main/coverage.svg)](#)
[![Release](https://gitlab.com/gitlab-org/cloud-native/operator/-/badges/release.svg)](#)

# GitLab Operator V2

NOTE:
GitLab Operator V2 is in early development stage and is not yet suitable for production use.

GitLab Operator V2 represents a significant rewrite and refactor of the existing
[GitLab Operator](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator).
Its aim is to improve the overall user experience for provisioning and managing
cloud-native GitLab applications on Kubernetes and OpenShift. It seeks to
supersede the existing GitLab Operator.

This new Operator incorporates fresh ideas and learnings from real-world usage
of the previous Operator to address the fundamental design issues and known
shortcomings. It uses a structured resource specification and is independent
from [GitLab Chart](https://gitlab.com/gitlab-org/charts/gitlab) deployment
model.

Architecturally, this new Operator moves towards a simplified yet more flexible
model optimized for operating cloud-native GitLab instances. In that sense, it
may depart from the existing cloud-native and hybrid deployment models of GitLab.

From an end-user perspective, this new Operator aims to provide a smooth and
familiar experience for managing the lifecycle of GitLab instances on Kubernetes.
It represents a leap forward in using Kubernetes best practices to deliver a
premier, cloud-native experience for operating GitLab.

## Goals

The overarching goal is to provide a smooth and familiar experience for GitLab
users to provision and manage cloud-native GitLab instances on Kubernetes.
More specifically, it:

- Provides an intuitive and familiar user experience for managing the lifecycle
  of cloud-native GitLab applications on Kubernetes, like any other native
  Kubernetes resource. It hides GitLab complexities from end users.
- Deeply integrates GitLab into the Kubernetes ecosystem by adhering to its
  conventions, best practices, and terminology. It enables GitLab applications to be
  first-class citizens on Kubernetes.
- Maintains a consistent user experience even as the underlying Kubernetes API
  and GitLab applications change over time.
