package framework

import (
	"errors"
	"fmt"
	"time"

	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

// Expand translates the error into a reconcile.Result and/or an error.
//
// If the error is returned from Requeue or RequeueWithDelay, a non-empty
// reconcile.Result is returned which requeues the request. Otherwise, the
// original error is returned.
//
// Note that an error that is returned from Terminate is not requeued, instead
// it terminates the reconcile request with the given error as the cause.
//
// When the error is nil it signals the successful termination of the reconcile
// loop.
func Expand(e error) (reconcile.Result, error) {
	re := &requeueError{}
	if errors.As(e, &re) {
		if re.delay == 0 {
			return reconcile.Result{Requeue: true}, nil
		} else {
			return reconcile.Result{RequeueAfter: re.delay}, nil
		}
	}

	return reconcile.Result{}, e
}

// Terminate terminates the reconcile loop with the given error.
func Terminate(e error) error {
	return reconcile.TerminalError(e)
}

// Requeue returns an error that once translated with Expand, requeues the request.
func Requeue() error {
	return &requeueError{}
}

// RequeueWithDelay returns an error that once translated with Expand, requeue
// the request after the specified delay.
func RequeueWithDelay(delay time.Duration) error {
	return &requeueError{delay: delay}
}

type requeueError struct {
	delay time.Duration
}

func (e *requeueError) Error() string {
	return fmt.Sprintf("requeue request after %s", e.delay)
}

// IsTerminal is true when err is a reconcile terminal error.
func IsTerminal(err error) bool {
	return errors.Is(err, reconcile.TerminalError(nil))
}
