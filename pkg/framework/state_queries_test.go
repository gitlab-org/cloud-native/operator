package framework

import (
	"errors"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/internal"
)

var _ = Describe("Queries", func() {
	var (
		rtCtx *RuntimeContext
	)

	BeforeEach(func() {
		rtCtx = &RuntimeContext{
			State: &mockSharedState{},
		}
	})

	Describe("SummonByType", func() {
		It("should return all values of the specified type", func() {
			query := &SummonByType[string]{}
			result, err := query.Get(rtCtx)

			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(ConsistOf("value1", "value2", "value3", "value4"))
		})

		It("should return ErrNotFound when no values of the specified type exist", func() {
			query := &SummonByType[int]{}
			result, err := query.Get(rtCtx)

			Expect(errors.Is(err, ErrNotFound)).To(BeTrue())
			Expect(result).To(BeEmpty())
		})
	})

	Describe("SummonByName", func() {
		It("should return the value associated with the given key", func() {
			query := &SummonByName[string]{Name: "key1"}
			result, err := query.Get(rtCtx)

			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(ConsistOf("value1"))
		})

		It("should return the values associated with the given key", func() {
			query := &SummonByName[string]{Name: "key3"}
			result, err := query.Get(rtCtx)

			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(ConsistOf("value3", "value4"))
		})

		It("should return ErrNotFound when the key doesn't exist", func() {
			query := &SummonByName[string]{Name: "nonexistent"}
			result, err := query.Get(rtCtx)

			Expect(errors.Is(err, ErrNotFound)).To(BeTrue())
			Expect(result).To(BeEmpty())
		})

		It("should return an error when the value type doesn't match", func() {
			query := &SummonByName[int]{Name: "key1"}
			result, err := query.Get(rtCtx)

			Expect(err).To(HaveOccurred())
			Expect(err.Error()).To(ContainSubstring("expected value of type int or []int, got string"))
			Expect(result).To(BeEmpty())
		})
	})

	Describe("SummonByTags", func() {
		It("should return values with all specified tags", func() {
			query := &SummonByTags[string]{Tags: []string{"tag1", "tag2"}}
			result, err := query.Get(rtCtx)

			Expect(err).NotTo(HaveOccurred())
			Expect(result).To(ConsistOf("value1"))
		})

		It("should return ErrNotFound when no values match all tags", func() {
			query := &SummonByTags[string]{Tags: []string{"tag1", "tag4"}}
			result, err := query.Get(rtCtx)

			Expect(errors.Is(err, ErrNotFound)).To(BeTrue())
			Expect(result).To(BeEmpty())
		})

		It("should return ErrNotFound when query tag list is empty", func() {
			query := &SummonByTags[string]{Tags: []string{}}
			result, err := query.Get(rtCtx)

			Expect(errors.Is(err, ErrNotFound)).To(BeTrue())
			Expect(result).To(BeEmpty())
		})
	})
})

// Mock implementation of the State interface for testing.
type mockSharedState struct{}

func (m *mockSharedState) Store(name string, value any, tags ...string) {}

func (m *mockSharedState) Get(key string) (interface{}, bool) {
	switch key {
	case "key1":
		return "value1", true
	case "key2":
		return "value2", true
	case "key3":
		return []string{"value3", "value4"}, true
	default:
		return nil, false
	}
}

func (m *mockSharedState) Visit(f internal.SharedStateVisitor) {
	f("key1", "value1", "tag1", "tag2")
	f("key2", "value2", "tag2")
	f("key3", []string{"value3", "value4"}, "tag3")
}
