package framework

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/internal"
)

var _ = Describe("SharedState", func() {
	var sharedState SharedState

	BeforeEach(func() {
		sharedState = internal.StateRegistry{}
	})

	It("should store and retrieve values", func() {
		sharedState.Store("key1", "value1", "tag1", "tag2")
		sharedState.Store("key2", 42, "tag2", "tag3")

		var retrievedValues = make(map[string]interface{})
		var retrievedTags = make(map[string][]string)

		sharedState.Visit(func(key string, value interface{}, tags ...string) bool {
			retrievedValues[key] = value
			retrievedTags[key] = tags
			return true
		})

		Expect(retrievedValues).To(HaveLen(2))
		Expect(retrievedValues["key1"]).To(Equal("value1"))
		Expect(retrievedValues["key2"]).To(Equal(42))

		Expect(retrievedTags).To(HaveLen(2))
		Expect(retrievedTags["key1"]).To(ConsistOf("tag1", "tag2"))
		Expect(retrievedTags["key2"]).To(ConsistOf("tag2", "tag3"))
	})

	It("should stop visiting when the visitor function returns false", func() {
		sharedState.Store("key1", "value1")
		sharedState.Store("key2", "value2")
		sharedState.Store("key3", "value3")

		var visitCount int
		sharedState.Visit(func(key string, value interface{}, tags ...string) bool {
			visitCount++
			return visitCount < 2
		})

		Expect(visitCount).To(Equal(2))
	})

	It("should handle empty tags", func() {
		sharedState.Store("key1", "value1")

		var retrievedTags []string
		sharedState.Visit(func(key string, value interface{}, tags ...string) bool {
			retrievedTags = tags
			return true
		})

		Expect(retrievedTags).To(BeEmpty())
	})

	It("should overwrite existing keys", func() {
		sharedState.Store("key1", "value1", "tag1")
		sharedState.Store("key1", "value2", "tag2")

		var retrievedValue interface{}
		var retrievedTags []string

		sharedState.Visit(func(key string, value interface{}, tags ...string) bool {
			retrievedValue = value
			retrievedTags = tags
			return true
		})

		Expect(retrievedValue).To(Equal("value2"))
		Expect(retrievedTags).To(ConsistOf("tag2"))
	})
})
