package inventory

import (
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// Inventory is a form of ContextualValue that provides Kubernetes resources.
// Controllers use it to produce their managed resources. It can apply to
// anything that creates or mutates Kubernetes objects, such as a Builder or
// a Factory.
type Inventory[T client.Object] framework.ContextualValue[T]

// InventoryFunc is a functional implementation of Inventory interface.
type InventoryFunc[T client.Object] func(*framework.RuntimeContext) ([]T, error)

// Get implements Inventory interface.
func (f InventoryFunc[T]) Get(rtCtx *framework.RuntimeContext) ([]T, error) {
	return f(rtCtx)
}

// AggregateInventory is an aggregator for multiple Inventory instances.
type AggregateInventory[T client.Object] []Inventory[T]

// Get implements Inventory interface.
func (a AggregateInventory[T]) Get(rtCtx *framework.RuntimeContext) ([]T, error) {
	objects := []T{}

	for _, i := range a {
		if i == nil {
			continue
		}

		o, err := i.Get(rtCtx)
		if err != nil {
			return objects, err
		}

		objects = append(objects, o...)
	}

	return objects, nil
}
