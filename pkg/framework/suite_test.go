package framework

import (
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestGitLabControllerFramework(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Controller Framework")
}
