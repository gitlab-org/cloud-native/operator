package kube

import (
	"bytes"
	"fmt"
	"io"

	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/yaml"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

// ReadObjects reads and decodes Kubernetes objects from a given io.Reader. It
// supports reading multiple objects from a single YAML or JSON source.
func ReadObjects(b io.Reader, decoder runtime.Decoder) ([]client.Object, error) {
	result := []client.Object{}

	// This decoder makes it possible to read multiple objects from a single
	// source.
	d := yaml.NewYAMLOrJSONDecoder(b, 4096)

	for {
		// We read the raw format first, then try to decode it.
		x := runtime.RawExtension{}
		if err := d.Decode(&x); err != nil {
			if err == io.EOF {
				return result, nil
			}

			return result, err
		}

		x.Raw = bytes.TrimSpace(x.Raw)
		if len(x.Raw) == 0 {
			continue
		}

		rtObj, gvk, err := decoder.Decode(x.Raw, nil, nil)
		if err != nil {
			return result, err
		}

		// We catch missing REST mapping earlier in order to avoid passing an
		// unknown object through the pipeline.
		if framework.Cluster != nil {
			_, err = framework.Cluster.GetRESTMapper().RESTMapping(gvk.GroupKind(), gvk.Version)
			if err != nil {
				if _, ok := err.(*meta.NoKindMatchError); ok {
					return result, fmt.Errorf("resource mapping not found for %q: %w", gvk, err)
				}

				return result, fmt.Errorf("unable to recognize %q: %w", gvk, err)
			}
		}

		// We also make sure that we can access object metadata.
		metav1Obj, err := meta.Accessor(rtObj)
		if err != nil {
			return result, err
		}

		// The following assertion MUST work, because:
		//   1. We fed runtime.Object to meta.Accessor
		//   2. We received an metav1.Object from meta.Accessor
		obj, ok := metav1Obj.(client.Object)
		if !ok {
			panic(fmt.Sprintf("engine failure: expected %T, got %T", obj, metav1Obj))
		}

		result = append(result, obj)
	}
}
