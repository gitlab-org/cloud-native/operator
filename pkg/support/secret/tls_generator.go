package secret

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"net"
	"time"
)

type TLSGenerator struct {
	Algorithm  Algorithm
	Size       int
	Lifespan   time.Duration
	CommonName string
	Hosts      []string
}

func (g *TLSGenerator) Generate() (Payload, error) {
	if err := validateAlgorithmSize(g.Algorithm, g.Size); err != nil {
		return nil, err
	}

	priv, err := GeneratePrivateKey(g.Algorithm, g.Size)
	if err != nil {
		return nil, err
	}

	// ECDSA, ED25519 and RSA subject keys should have the DigitalSignature
	// KeyUsage bits set in the x509.Certificate template
	// Also make this cert its own Certificate Authority
	keyUsage := x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign

	// Only RSA subject keys should have the KeyEncipherment KeyUsage bits set.
	// In the context of TLS this KeyUsage is particular to RSA key exchange and
	// authentication.
	if _, isRSA := priv.(*rsa.PrivateKey); isRSA {
		keyUsage |= x509.KeyUsageKeyEncipherment
	}

	notBefore := nowFunc()
	notAfter := notBefore.Add(g.Lifespan)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 20*8)

	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return nil, err
	}

	// Add 1 to avoid a serial number of 0
	serialNumber = serialNumber.Add(serialNumber, big.NewInt(1))

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			CommonName: g.CommonName,
		},
		Issuer: pkix.Name{
			CommonName: g.CommonName,
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		IsCA:        true,
		KeyUsage:    keyUsage,
		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},

		BasicConstraintsValid: true,
	}

	for _, h := range g.Hosts {
		if ip := net.ParseIP(h); ip != nil {
			template.IPAddresses = append(template.IPAddresses, ip)
		} else {
			template.DNSNames = append(template.DNSNames, h)
		}
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, GetPublicKey(priv), priv)
	if err != nil {
		return nil, err
	}

	privBytes, err := x509.MarshalPKCS8PrivateKey(priv)
	if err != nil {
		return nil, err
	}

	var certOut bytes.Buffer
	if err = pem.Encode(&certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
		return nil, fmt.Errorf("failed to encode X509 certificate in PEM format: %w", err)
	}

	var keyOut bytes.Buffer
	if err = pem.Encode(&keyOut, &pem.Block{Type: "PRIVATE KEY", Bytes: privBytes}); err != nil {
		return nil, fmt.Errorf("failed to encode X509 key in PEM format: %w", err)
	}

	return Payload{TLSCertificate: certOut.Bytes(), TLSPrivateKey: keyOut.Bytes()}, nil
}

var (
	nowFunc = time.Now
)
