package secret

import (
	"bytes"
	"crypto/x509"
	"encoding/pem"
	"fmt"
)

type PrivateKeyGenerator struct {
	Algorithm Algorithm
	Size      int
	Names     []string
}

func (g *PrivateKeyGenerator) Generate() (Payload, error) {
	if err := validateAlgorithm(g.Algorithm); err != nil {
		return nil, err
	}

	if err := validateAlgorithmSize(g.Algorithm, g.Size); err != nil {
		return nil, err
	}

	result := Payload{}

	for _, name := range g.Names {
		priv, err := GeneratePrivateKey(g.Algorithm, g.Size)
		if err != nil {
			return nil, err
		}

		privBytes, err := x509.MarshalPKCS8PrivateKey(priv)
		if err != nil {
			return nil, err
		}

		var privOut bytes.Buffer
		if err = pem.Encode(&privOut, &pem.Block{Type: "PRIVATE KEY", Bytes: privBytes}); err != nil {
			return nil, fmt.Errorf("failed to encode X509 key in PEM format: %w", err)
		}

		result[name] = privOut.Bytes()
	}

	return result, nil
}
