#!/usr/bin/env sh

set -u

cat << EOF 
# :tanuki: GitLab Operator V2 - ${CI_COMMIT_TAG}

* :notebook: [API documentation](https://gitlab.com/gitlab-org/cloud-native/operator/-/blob/${CI_COMMIT_TAG}/docs/generated/api-docs.md)
* :ship: Docker image: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} 

:warning: Note: This project is [experimental](https://docs.gitlab.com/ee/policy/experiment-beta-support.html) and is not ready for production use.
EOF
