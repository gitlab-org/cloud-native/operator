package main

import (
	"fmt"

	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/kustomize/api/krusty"
	"sigs.k8s.io/kustomize/kyaml/filesys"
)

func readKustomize(path string) ([]client.Object, error) {
	var objects []client.Object

	fSys := filesys.MakeFsOnDisk()
	k := krusty.MakeKustomizer(krusty.MakeDefaultOptions())

	resMap, err := k.Run(fSys, path)
	if err != nil {
		return nil, fmt.Errorf("failed to run kustomize: %w", err)
	}

	for _, resource := range resMap.Resources() {
		yaml, err := resource.AsYAML()
		if err != nil {
			return nil, fmt.Errorf("failed to convert resource to yaml: %w", err)
		}

		obj, _, err := decoder.Decode(yaml, nil, nil)
		if err != nil {
			return nil, fmt.Errorf("failed to decode yaml: %w", err)
		}

		clientObj, ok := obj.(client.Object)
		if !ok {
			return nil, fmt.Errorf("failed to cast object to client.Object")
		}

		objects = append(objects, clientObj)
	}

	return objects, nil
}
