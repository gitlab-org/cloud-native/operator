package main

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/spf13/cobra"
)

var updateCmd = &cobra.Command{
	Use:     "update old new",
	Short:   "Run validation on an update operation",
	Example: "  validation -c config/crd -l debug update old.yaml new.yaml",
	Args:    cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		if err := validateUpdate(args[0], args[1]); err != nil {
			os.Exit(1)
		}
	},
}

func validateUpdate(oldPath, newPath string) error {
	vc, err := NewValidatorCollection(crdPath)
	if err != nil {
		slog.Error("Failed to create validator collection", "error", err)
		return err
	}

	old, err := readUnstructuredObjects(oldPath)
	if err != nil {
		slog.Error("Failed to read old resource", "error", err)
		return err
	}

	new, err := readUnstructuredObjects(newPath)
	if err != nil {
		slog.Error("Failed to read new resource", "error", err)
		return err
	}

	if len(new) != len(old) {
		slog.Error("Resource count mismatch", "old", len(old), "new", len(new))
		return fmt.Errorf("resource count mismatch, len(old)=%d, len(new)=%d", len(old), len(new))
	}

	for objIdx := range new {
		errs := vc.ValidateCustomResourceUpdate(old[objIdx], new[objIdx])

		header := fmt.Sprintf("%s#%d:", newPath, objIdx)

		if len(errs) == 0 {
			fmt.Printf("%s %s %s:\n", greenColor(header), new[objIdx].GroupVersionKind().String(), new[objIdx].GetName())
			fmt.Printf("\t%s\n", greenColor("PASS"))
		} else {
			fmt.Printf("%s %s %s:\n", redColor(header), new[objIdx].GroupVersionKind().String(), new[objIdx].GetName())
		}

		for errIdx, err := range errs {
			fmt.Printf("\t%s #%d: %s\n", yellowColor("ERROR"), errIdx, err.Error())
		}
	}

	return nil
}
