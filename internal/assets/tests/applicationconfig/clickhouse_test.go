package applicationconfig

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _clickhouse.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/clickhouse.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()
			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleClickHouse, "clickhouse/simple.yaml"),
	)
})

var (
	simpleClickHouse = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				ClickHouse: []v2alpha2.ClickHouse{
					{
						Name: "main",
						ServiceProvider: v2alpha2.ServiceProvider{
							Authentication: v2alpha2.ServiceAuthentication{
								Basic: &corev1.SecretReference{},
							},
						},
					},
				},
			}},
		"ClickHouseSecrets": map[string]*corev1.Secret{
			"main": {
				Data: map[string][]byte{
					"username": []byte("username"),
					"password": []byte("password"),
				},
			},
		},
		"ClickHouseEndpoints": map[string]apiutils.ServiceEndpoint{
			"main": {
				Scheme: "https",
				Host:   "clickhouse.example.com",
				Port:   443,
			},
		},
	}
)
