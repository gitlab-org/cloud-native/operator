package applicationconfig

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _kerberos.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/kerberos.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()
			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleKerberos, "kerberos/simple.yaml"),
	)
})

var (
	simpleKerberos = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				Kerberos: &v2alpha2.ApplicationKerberosSettings{
					GSSAPISupport: v2alpha2.GSSAPISupport{
						Keytab: corev1.SecretKeySelector{
							LocalObjectReference: corev1.LocalObjectReference{Name: "foo"},
							Key:                  "bar",
						},
						ServicePrincipalName: "principal@example.com",
					},
					SimpleLDAPLinkingAllowedRealms: []string{"realm1", "realm2"},
					DedicatedPort:                  &v2alpha2.KerberosDedicatedPort{Port: 8443, HTTPS: true},
				},
			},
		},
	}
)
