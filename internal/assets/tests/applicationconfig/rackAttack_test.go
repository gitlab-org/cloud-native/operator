package applicationconfig

import (
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"k8s.io/utils/ptr"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/inventory"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("Partial: _rackAttack.yaml", func() {
	var (
		testTemplate string = "application-config/testdata/rackAttack.yaml"
	)

	DescribeTable("render partial",
		func(context map[string]any, output string) {
			context["Settings"] = settings.Get()

			data, err := inventory.RenderTemplate(testTemplate, context)
			Expect(err).ToNot(HaveOccurred())

			rendered := data[testTemplate].String()

			actual := readYAMLData([]byte(rendered))
			expected := readYAMLSource(output)

			Expect(actual).To(BeComparableTo(expected))
		},
		Entry("with simple configuration", simpleRackAttack, "rackAttack/simple.yaml"),
		Entry("with full configuration", fullRackAttack, "rackAttack/full.yaml"),
	)
})

var (
	simpleRackAttack = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				RackAttackGitBasicAuth: &v2alpha2.ApplicationRackAttackGitBasicAuth{
					IPWhitelist: []string{"127.0.0.1"},
				},
			},
		},
	}

	fullRackAttack = map[string]any{
		"ApplicationConfig": v2alpha2.ApplicationConfig{
			Spec: v2alpha2.ApplicationConfigSpec{
				RackAttackGitBasicAuth: &v2alpha2.ApplicationRackAttackGitBasicAuth{
					IPWhitelist: []string{"127.0.0.1", "127.0.0.2"},
					MaxRetry:    ptr.To(int32(10)),
					FindTime:    &metav1.Duration{Duration: time.Minute},
					BanTime:     &metav1.Duration{Duration: time.Hour},
				},
			},
		},
	}
)
