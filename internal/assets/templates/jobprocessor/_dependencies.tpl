{{- $spec := .JobProcessor.Spec -}}
{{- $appName := printf "%s-%s" .JobProcessor.Namespace .JobProcessor.Name -}}

image: {{ getImageName $spec.Workload.ImageSource "Sidekiq" $spec.Version $spec.Edition }}
args:
  - /scripts/wait-for-deps
env:
  - name: CONFIG_DIRECTORY
    value: {{ .Settings.Rails.ConfigDir }}
  - name: ENABLE_BOOTSNAP
    value: {{ $spec.Sidekiq.Settings.EnableBootsnap | default .Settings.Rails.EnableBootsnap }}
volumeMounts:
  {{- include "jobprocessor/_railsVolumeMount.tpl" . | nindent 2 }}

{{- include "commons/_defaultResources.tpl" .Settings.Rails }}
{{- include "commons/_defaultContainerSecurityContext.tpl" .Settings.Rails }}
