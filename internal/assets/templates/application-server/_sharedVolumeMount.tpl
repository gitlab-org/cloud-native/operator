- name: secrets
  mountPath: {{ .Settings.Secrets.MountPath }}
- name: rails-metrics
  mountPath: {{ .Settings.Rails.MetricsDir }}
- name: shared-upload
  mountPath: {{ .Settings.Rails.UploadDir }}
- name: shared-temp
  mountPath: {{ .Settings.Rails.TempDir }}
{{- with .ApplicationConfig.Spec.Kerberos }}
- name: secrets
  mountPath: /etc/krb5.keytab
  subPath: {{ .Keytab.Key }}
{{- end }}