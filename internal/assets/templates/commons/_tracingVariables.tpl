{{/*
Prints out GitLab tracing variables.

Expects a context with the following keys:
  - Tracing: The tracing information from TracingSupport API type. 
*/}}
{{- with .Tracing }}
- name: GITLAB_TRACING
  value: {{ .ConnectionString | quote }}
- name: GITLAB_TRACING_URL
  value: {{ .URLTemplate | quote }}
{{- end }}