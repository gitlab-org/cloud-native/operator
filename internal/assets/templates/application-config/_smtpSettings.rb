{{/* 
Renders SMTP settings initializer for Rails.

Expects a context with the following keys:
  - ApplicationConfig: The ApplicationConfig custom resource.
  - SMTPCredentials:  The Secret containing SMTP credentials.
  - SMTPEndpoint: The ServiceEndpoints for SMTP provider.
*/}}

{{- $ := . -}}

{{- with .ApplicationConfig.Spec.OutgoingEmail }}
  {{- with .Outbox.SMTP }}
smtp_settings = {
  address: '{{ $.SMTPEndpoint.Host }}',
  port: {{ $.SMTPEndpoint.Port }},
  user_name: '{{ getSecretValue $.SMTPCredentials "username" }}',
  password: '{{ getSecretValue $.SMTPCredentials "password" }}',
  {{- with .TLS }}
    tls: true,
    ca_file: '{{ $.Settings.Secrets.MountPath }}/smtp/ca.crt',
  {{- end }}  
  {{- range $key, $value := (mapSMTPSettings .Settings) }}
    {{ $key }}: {{ $value }},
  {{- end }}
}

    {{- if .Pool }}
require 'mail/smtp_pool'

Rails.application.config.action_mailer.delivery_method = :smtp_pool

ActionMailer::Base.delivery_method = :smtp_pool
ActionMailer::Base.smtp_pool_settings = {
  pool: Mail::SMTPPool.create_pool(
    smtp_settings.merge(pool_size: Gitlab::Runtime.max_threads)
  )
}
    {{- else }}
Rails.application.config.action_mailer.delivery_method = :smtp

ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = smtp_settings
    {{- end -}}
  {{- end -}}
{{- end -}}
