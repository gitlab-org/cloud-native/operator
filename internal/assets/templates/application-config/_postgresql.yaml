{{/* 
Renders the PostgreSQL configuration for Rails.

Expects a context with the following keys:
  - ApplicationConfig: The ApplicationConfig custom resource.
  - PostgreSQLSecrets: A map of Secret objects, with name of the PostgreSQL
    entry as its key and the Secret object of the authentication as its value.
    Only basic authentication Secrets are used to render the template.
  - PostgreSQLEndpoints: A map of ServiceEndpoints, with name of the PostgreSQL
    entry as its key.
*/}}

{{- $ := . -}}
{{- $dbConfig := mapPostgreSQLDatabases .ApplicationConfig.Spec.PostgreSQL -}}

{{ .Settings.Rails.Environment }}:
{{- range $dbConfig }}
  {{- $db := .PostgreSQL -}}
  {{- $mountPath := printf "%s/postgresql/%s" $.Settings.Secrets.MountPath ($db.Name | snakecase) }}
  {{- $authSecret := index $.PostgreSQLSecrets $db.Name }}
  {{- $endpoint := index $.PostgreSQLEndpoints $db.Name }}
  {{ .Key }}:
    adapter: postgresql
    host: {{ $endpoint.Host }}
    port: {{ $endpoint.Port }}
    username: {{ $db.Username | default (getSecretValue $authSecret "username") | default $.Settings.Rails.PostgreSQLUsername }}
    database: {{ $db.DatabaseName | default $.Settings.Rails.PostgreSQLDatabase }}
    sslmode: {{ postgresqlSSLMode $db }}

    {{- with $db.Authentication.Basic }}
    password: {{ getSecretValue $authSecret "password" | quote }}
    {{- end }}

    {{- with $db.Authentication.ClientTLS }}
    sslcert: {{ printf "%s/client/tls.crt" $mountPath }}
    sslkey: {{ printf "%%s/client/tls.key" $mountPath }}
      {{- if .CACertificate }}
    sslrootcert: {{ printf "%s/client/ca.crt" $mountPath }}
      {{- end }}
    {{- end }}

    {{- with $db.TLS }}
    sslrootcert: {{ printf "%s/ca.crt" $mountPath }}
      {{- with .MinVersion -}}
    ssl_min_protocol_version: {{ . }}
      {{- end }}
      {{- with .MaxVersion -}}
    ssl_max_protocol_version: {{ . }}
      {{- end }}
    {{- end }}

    prepared_statements: false
    database_tasks: {{ not (eq .Key "ci") }}
    {{- with $db.Settings }}
    {{- mapPostgreSQLSettings . | toYaml | nindent 4 }}
    {{- end }}
{{- end }}