// This package provides an internal API layer that serves as an intermediary
// between versioned public API types and controller logic in a Kubernetes-based
// system.
//
// The package offers a set of interfaces and types that create "shadow"
// representations of public API types, which controllers can consume. These
// shadow types provide additional features and flexibility while reducing
// direct dependency on potentially changing public API structures.
//
// Key features of this package include:
//
//   - Composition of complex types from simpler building blocks
//   - Handling of latent and referenced values
//   - Lazy loading of referenced resources
//   - De-referencing of Kubernetes object references
//   - Management of resources that may become available eventually
//
// While the internal API does not completely abstract public API types, it acts
// as a buffer that absorbs some changes in the public API, reducing the impact
// of such changes on controllers.
//
// The core interfaces and types in this package include:
//
//   - Element: Represents a single unit or composition of units in the API
//     structure
//   - Composite: An Element that can contain child elements
//   - Resource: Consumes values to update its state
//   - Reference: Resolves itself into a concrete value
//
// Some of the basic data types that can be used as building blocks are:
//
//   - SecretData: A string-valued map Represents payload of a Kubernetes Secret.
//   - ServiceEndpoint: A summary of key details of a v2alpha2.ServiceEndpoint.
//   - ServiceProvider: A composite resource that expands v2alpha2.ServiceProvider.
//
// This package is particularly useful for working with referenced Kubernetes
// objects such as Secrets and Services, resolving these references, and
// handling missing Kubernetes resources.
//
// Example: ApplicationConfig
//
// The ApplicationConfig type demonstrates how this package can be used to create
// an internal representation of a public API type. It embeds the public
// v2alpha2.ApplicationConfig type and expands it with additional functionality:
//
//	type ApplicationConfig struct {
//	    Composite
//	    *v2alpha2.ApplicationConfig
//
//	    PostgreSQL       *NamedServiceProvider
//	    Prometheus       *ServiceEndpoint
//	    ZoektCredentials SecretData
//
//	    // Other attributes
//	}
//
// This structure allows for lazy loading of referenced resources (PostgreSQL,
// Prometheus, ZoektCredentials) and provides a flexible way to declare which
// resources should be loaded.
//
// Usage example:
//
//	appConfig := NewApplicationConfig()
//
//	err := appConfig.Consume(ctx, &v2alpha2.ApplicationConfig{
//	    // ... retrieved from Kubernetes API
//	})
//
//	if !appConfig.Ready() {
//	    // Some resources are not yet available. Requeue and try again
//	}
//
//	// Access resolved resources, either in controller code or templates:
//
//	mainPostgreSQLEndpoint := appConfig.PostgreSQL.All["main"].Endpoint
//	prometheusHost := appConfig.Prometheus.Host
//	zoektUsername := appConfig.ZoektCredentials["username"]
//
// This example demonstrates how the package allows for easy consumption of
// public API types, resolution of referenced resources, and access to the
// resolved data in a type-safe manner.
package api
