package api

import (
	"context"
	"fmt"

	kerrors "k8s.io/apimachinery/pkg/api/errors"
)

// Element represents a single unit or a composition of units in the hierarchical
// structure of the internal API types.
//
// This interface allows for uniform treatment of both simple and complex
// elements, enabling clients to work with individual objects and compositions
// of objects transparently.
//
// An Element is generally loaded from a versioned public API type. Its design
// adds support for latent and referenced values. It means that it is capable of
// de-referencing a Kubernetes object reference and handle resources that are
// "eventually" become available.
//
// It is used as part of the composite pattern to build and reuse internal API
// data structures.
type Element interface {
	// Load refreshes the Element from an underlying value.
	//
	// This method performs the following tasks:
	//
	//   1. Loads the value of an element from a public API type.
	//   2. Resolves any references within the element structure.
	//   3. Loads resources from the resolved referenced values.
	//   4. For composite elements, recursively loads all child elements.
	//
	// This approach enables:
	//
	//   - Lazy loading of referenced resources
	//   - Dynamic composition of internal API types
	//
	// Returns an error if any part of the loading process fails, including
	// failures in reference resolution or loading of referenced resources.
	Load(context.Context) error

	// Ready signals if the element is ready to use. Useful when the element is
	// referencing a Kubernetes object that is not readily available.
	Ready() bool
}

// Composite represents an element that can contain child elements. It extends
// the Element interface with additional functionality.
type Composite interface {
	Element

	// Missing returns a list of child elements that are not ready.
	Missing() []string
}

// Resource consumes value of T to update its state using the given context.
// The value of T can be retrieved from the resolution of a Reference.
//
// Concrete resources can form the elements of a Composite. See [AddResource]
// function.
type Resource[T any] interface {
	// Consumes the given value of T. In case of any failure returns an error.
	// Critical errors result in a terminal error.
	Consume(context.Context, T) error
}

// Reference resolves itself into a concrete value of T using the given context.
// The resulting value is generally passed to a Resource to consume it.
//
// A pair of [Reference] and [Resource] can form an element of a Composite. See
// [AddReferencedResource] function.
type Reference[T any] interface {
	// Resolves the reference into a value of T. In case of any failure returns
	// an error. Critical errors result in a terminal error.
	Resolve(context.Context) (T, error)
}

// NamedResources is a collection of named Resources that consumes a collection
// of named values of T. The collection is map of string to T.
type NamedResources[T any, R Resource[T]] struct {
	Composite

	All     map[string]R
	builder func(context.Context) R
}

// NewNamedResources creates a new NamedResources using the generator function.
// The function is used to create a new concrete Resource type.
func NewNamedResources[T any, R Resource[T]](builder func(context.Context) R) *NamedResources[T, R] {
	return &NamedResources[T, R]{
		Composite: NewComposite(),
		All:       map[string]R{},
		builder:   builder,
	}
}

func (n *NamedResources[T, R]) Consume(ctx context.Context, collection map[string]T) error {
	for k, v := range collection {
		if _, exists := n.All[k]; !exists {
			n.All[k] = n.builder(ctx)
		}

		AddResource(n.Composite, v, n.All[k])
	}

	return n.Load(ctx)
}

// NamedReferences is a collection of named References which is a Reference
// itself. It resolves to a map of named values of concrete type T.
type NamedReferences[T any] map[string]Reference[T]

func (n NamedReferences[T]) Resolve(ctx context.Context) (map[string]T, error) {
	result := map[string]T{}

	for k, v := range n {
		t, err := v.Resolve(ctx)
		if err != nil {
			return nil, err
		}

		result[k] = t
	}

	return result, nil
}

// SelfReference is a value that resolves to itself. It implements Reference[T]
// interface and is used by resource composition machinery.
type SelfReference[T any] struct {
	Value T
}

func (s *SelfReference[T]) Resolve(_ context.Context) (T, error) {
	return s.Value, nil
}

func (s *SelfReference[T]) String() string {
	return fmt.Sprintf("SelfReference<%T>", s.Value)
}

func NewSelfReference[T any](value T) Reference[T] {
	return &SelfReference[T]{Value: value}
}

// NewComposite creates and returns a new Composite instance.
func NewComposite() Composite {
	return &defaultComposite{
		missing:    map[string]bool{},
		children:   []Element{},
		isNotFound: kerrors.IsNotFound,
	}
}

// Add a child Element to the given Composite.
func AddElement(composite Composite, element Element) {
	if defaultComposite, ok := composite.(*defaultComposite); ok {
		defaultComposite.children = append(defaultComposite.children, element)
	}
}

// AddResource adds a resource to the given Composite that is loaded from a
// static value.
func AddResource[T any](composite Composite, value T, resource Resource[T]) {
	AddReferencedResource(composite, NewSelfReference(value), resource)
}

// AddResource adds a pair of reference and resource to the given Composite that
// for a referenced value.
func AddReferencedResource[T any](composite Composite, reference Reference[T], resource Resource[T]) {
	if defaultComposite, ok := composite.(*defaultComposite); ok {
		if _, ok := resource.(Composite); ok {
			defaultComposite.addComposite(newAnyReference(reference), newAnyCompositeResource(resource))
		} else {
			defaultComposite.add(newAnyReference(reference), newAnyResource(resource),
				fmt.Sprintf("%s", reference))
		}
	}
}

/*
 * Hiding all the generic shenanigans and type erasure behind the interfaces.
 */

type anyReference func(context.Context) (any, error)
type anyResource func(context.Context, any) (bool, error)
type anyCompositeResource func(context.Context, any) ([]string, error)

func newAnyReference[T any](reference Reference[T]) anyReference {
	return anyReference(func(ctx context.Context) (any, error) {
		return reference.Resolve(ctx)
	})
}

func newAnyResource[T any](resource Resource[T]) anyResource {
	return anyResource(func(ctx context.Context, val any) (bool, error) {
		t, isT := val.(T)
		if !isT {
			return false, fmt.Errorf("expected %T, got %T", t, val)
		}

		if err := resource.Consume(ctx, t); err != nil {
			return false, err
		}

		if element, ok := resource.(Element); ok && !element.Ready() {
			return false, nil
		}

		return true, nil
	})
}

func newAnyCompositeResource[T any](resource Resource[T]) anyCompositeResource {
	return anyCompositeResource(func(ctx context.Context, val any) ([]string, error) {
		t, isT := val.(T)
		if !isT {
			return nil, fmt.Errorf("expected %T, got %T", t, val)
		}

		if err := resource.Consume(ctx, t); err != nil {
			return nil, err
		}

		if element, ok := resource.(Composite); ok && !element.Ready() {
			return element.Missing(), nil
		}

		return nil, nil
	})
}

type defaultElement struct {
	reference anyReference
	resource  anyResource
	caption   string
	ready     bool
}

func (c *defaultElement) Load(ctx context.Context) error {
	val, err := c.reference(ctx)
	if err != nil {
		return err
	}

	if c.ready, err = c.resource(ctx, val); err != nil {
		return err
	}

	return nil
}

func (c *defaultElement) Ready() bool {
	return c.ready
}

func (c *defaultElement) String() string {
	return c.caption
}

type defaultCompositeElement struct {
	reference anyReference
	resource  anyCompositeResource
	missing   []string
}

func (c *defaultCompositeElement) Load(ctx context.Context) error {
	val, err := c.reference(ctx)
	if err != nil {
		return err
	}

	if c.missing, err = c.resource(ctx, val); err != nil {
		return err
	}

	return nil
}

func (c *defaultCompositeElement) Ready() bool {
	return len(c.missing) == 0
}

func (c *defaultCompositeElement) Missing() []string {
	if c.Ready() {
		return []string{}
	}

	return c.missing
}

type isNotFound func(error) bool

type defaultComposite struct {
	missing    map[string]bool
	children   []Element
	isNotFound isNotFound
}

func (c *defaultComposite) Load(ctx context.Context) error {
	for _, child := range c.children {
		if err := child.Load(ctx); err != nil || !child.Ready() {
			if err == nil || c.isNotFound(err) {
				if compositeChild, ok := any(child).(Composite); ok {
					for _, missing := range compositeChild.Missing() {
						c.missing[missing] = true
					}
				} else {
					c.missing[fmt.Sprintf("%s", child)] = true
				}

				continue
			}

			return err
		}
	}

	return nil
}

func (c *defaultComposite) Ready() bool {
	return len(c.missing) == 0
}

func (c *defaultComposite) Missing() []string {
	m := make([]string, 0, len(c.missing))
	for k := range c.missing {
		m = append(m, k)
	}

	return m
}

func (h *defaultComposite) add(reference anyReference, resource anyResource, caption string) {
	h.children = append(h.children, &defaultElement{
		reference: reference,
		resource:  resource,
		caption:   caption,
	})
}

func (h *defaultComposite) addComposite(reference anyReference, resource anyCompositeResource) {
	h.children = append(h.children, &defaultCompositeElement{
		reference: reference,
		resource:  resource,
	})
}
