package templateutils

import (
	"fmt"
	"math"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

func maxRequestDuration(workerTimeout *metav1.Duration, in metav1.Duration) metav1.Duration {
	if in.Duration != 0 {
		return in
	}

	w := settings.Get().Rails.Puma.WorkerTimeout
	if workerTimeout != nil {
		w = workerTimeout.Duration
	}

	r := math.Round(float64(w.Nanoseconds()) * defaultMaxRequestDurationFactor)

	return metav1.Duration{Duration: time.Duration(r) * time.Nanosecond}
}

// The following function flattens TLS Secrets for various extensions. It is
// used to projects them into the all-in-one `secrets` volume.
// It accepts an instance of `v2alpha2.ApplicationServer` or `v2alpha2.DatabaseMigration` as input.
// For `v2alpha2.DatabaseMigration` it returns a map with minimal TLS support for PostgreSQL and Redis clients.
//
// | Path                     | TLS Files                  | Description                  |
// |--------------------------|----------------------------|------------------------------|
// | puma/                    | tls.key, tls.crt, ca.crt   | Puma server                  |
// | puma/metrics/            | tls.key, tls.crt, ca.crt   | Puma metrics server          |
// | workhorse/               | tls.key, tls.crt, ca.crt   | Workhorse server             |
// | workhorse/metrics/       | tls.key, tls.crt, ca.crt   | Workhorse metrics server     |
// | postgresql/<name>        | ca.crt                     | PostgreSQL client            |
// | postgresql/<name>/client | tls.key, tls.crt, ca.crt   | PostgreSQL mTLS auth         |
// | redis/<name>             | ca.crt                     | Redis client                 |
// | redis/<name>/client      | tls.key, tls.crt, ca.crt   | Redis mTLS auth              |
// | smime/                   | tls.key, tls.crt, ca.crt   | Outgoing mail SMIME          |
// | smtp/                    | ca.crt                     | Outgoing mail SMTP client    |
// | imap/incoming/           | ca.crt                     | Incoming mail IMAP client    |
// | imap/servicedesk/        | ca.crt                     | ServiceDesk IMAP client      |
// | ldap/<name>              | ca.crt                     | LDAP client                  |
// | ldap/<name>/client       | tls.key, tls.crt, ca.crt   | LDAP mTLS auth               |
// | smartcard/               | ca.crt                     | Smartcard trust anchor       |
//
// NOTES:
//   - All entries are lower cased, including `<name>` part.
func populateTLSSupport(instance any, appConfig *v2alpha2.ApplicationConfig) (map[string]any, error) {
	switch instance := instance.(type) {
	case *v2alpha2.ApplicationServer:
		return populateAppServerTLS(instance, appConfig), nil
	case *v2alpha2.DatabaseMigration:
		return databaseTLSSupport(appConfig), nil
	default:
		return nil, fmt.Errorf("unsupported instance type: %T", instance)
	}
}

func databaseTLSSupport(appConfig *v2alpha2.ApplicationConfig) map[string]any {
	result := map[string]any{}
	for _, postgresql := range appConfig.Spec.PostgreSQL {
		result["postgresql/"+postgresql.Name] = postgresql.TLS
		if postgresql.Authentication.ClientTLS != nil {
			result["postgresql/"+postgresql.Name+"/client"] = &v2alpha2.CertificateBundle{
				Credentials: *postgresql.Authentication.ClientTLS,
			}
		}
	}

	for _, redis := range appConfig.Spec.Redis {
		result["redis/"+redis.Name] = redis.TLS
		if redis.Authentication.ClientTLS != nil {
			result["postgresql/"+redis.Name+"/client"] = &v2alpha2.CertificateBundle{
				Credentials: *redis.Authentication.ClientTLS,
			}
		}
	}

	return result
}

func populateAppServerTLS(instance *v2alpha2.ApplicationServer, appConfig *v2alpha2.ApplicationConfig) map[string]any {
	result := databaseTLSSupport(appConfig)

	result["puma"] = instance.Spec.Puma.TLS
	result["puma/metrics"] = instance.Spec.Puma.Metrics.TLS
	result["workhorse"] = instance.Spec.Workhorse.TLS
	result["workhorse/metrics"] = instance.Spec.Workhorse.Metrics.TLS

	if appConfig.Spec.LDAP != nil {
		for _, ldap := range appConfig.Spec.LDAP.Servers {
			result["ldap/"+ldap.Name] = ldap.TLS
			if ldap.Authentication.ClientTLS != nil {
				result["ldap/"+ldap.Name+"/client"] = &v2alpha2.CertificateBundle{
					Credentials: *ldap.Authentication.ClientTLS,
				}
			}
		}
	}

	if appConfig.Spec.OutgoingEmail != nil {
		if appConfig.Spec.OutgoingEmail.SMIME != nil {
			result["smime"] = appConfig.Spec.OutgoingEmail.SMIME
		}

		if appConfig.Spec.OutgoingEmail.Outbox.SMTP != nil {
			result["smtp"] = appConfig.Spec.OutgoingEmail.Outbox.SMTP.TLS
		}
	}

	if appConfig.Spec.IncomingEmail != nil && appConfig.Spec.IncomingEmail.Inbox.IMAP != nil {
		result["imap/incoming"] = appConfig.Spec.IncomingEmail.Inbox.IMAP.TLS
	}

	if appConfig.Spec.ServiceDeskEmail != nil && appConfig.Spec.ServiceDeskEmail.Inbox.IMAP != nil {
		result["imap/servicedesk"] = appConfig.Spec.ServiceDeskEmail.Inbox.IMAP.TLS
	}

	return result
}

const (
	defaultMaxRequestDurationFactor float64 = 0.95
)

var (
	cspDirectivesMapper = mapper{}

	railsSettingsMapper = mapper{
		keySet: []string{
			"defaultCanCreateGroup",
			"usernameChangingEnabled",
			"defaultTheme",
			"customHtmlHeaderTags",
			"issueClosingPattern",
			"impersonationEnabled",
			"disableAnimations",
		},
		valueMapping: map[string]func(string) any{
			"defaultCanCreateGroup":   toBool,
			"usernameChangingEnabled": toBool,
			"defaultTheme":            toIntWithFallback(1),
			"impersonationEnabled":    toBool,
			"disableAnimations":       toBool,
		},
	}

	defaultProjectFeaturesMapper = mapper{
		keySet: []string{
			"issues",
			"mergeRequests",
			"wiki",
			"snippets",
			"builds",
			"containerRegistry",
		},
	}

	smtpSettingsMapper = mapper{
		keySet: []string{
			"domain",
			"authentication",
			"autoStartTLS",
			"opensslVerifyMode",
			"openTimeout",
			"readTimeout",
		},
		keyMapping: map[string]func(string) string{
			"autoStartTLS": func(string) string {
				return "enable_starttls_auto"
			},
		},
		valueMapping: map[string]func(string) any{
			"domain":            toRubyString,
			"authentication":    toRubySymbol,
			"autoStartTLS":      toBool,
			"opensslVerifyMode": toRubyString,
			"openTimeout":       toInt,
			"readTimeout":       toInt,
		},
	}

	imapSettingsMapper = mapper{
		keySet: []string{
			"startTLS",
			"idleTimeout",
		},
		valueMapping: map[string]func(string) any{
			"startTLS":    toBool,
			"idleTimeout": toInt,
		},
	}
)
