package templateutils

import "gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"

func mapDestinationStores(objectStorage v2alpha2.ApplicationObjectStorage) map[v2alpha2.ObjectStoreUsage]v2alpha2.ObjectStore {
	result := map[v2alpha2.ObjectStoreUsage]v2alpha2.ObjectStore{}

	for _, appObj := range objectStorage.Objects {
		for _, store := range objectStorage.Stores {
			if store.Name == appObj.StoreName {
				result[appObj.Usage] = store

				break
			}
		}
	}

	return result
}
