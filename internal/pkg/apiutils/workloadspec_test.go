package apiutils

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/equality"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/yaml"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
)

var _ = Describe("WorkloadSpec", func() {
	workloadspec := &v2alpha2.WorkloadSpec{
		ImageSource: &v2alpha2.ImageSource{
			Registry:    "registry.example.com",
			Repository:  "project/image",
			Flavor:      v2alpha2.UBI,
			PullSecrets: []corev1.LocalObjectReference{{Name: "pull-secret"}},
		},

		PodTemplate: &v2alpha2.PodTemplate{
			Metadata: &v2alpha2.PodMetadata{
				Labels: map[string]string{
					"custom-label": "custom-value",
				},
				Annotations: map[string]string{
					"custom-annotation": "custom-value",
				},
			},

			Spec: &v2alpha2.RestrictedPodSpec{
				ExtraContainers: []corev1.Container{
					{Name: "extra_container", Image: "alpine:latest"},
				},
				ExtraInitContainers: []corev1.Container{
					{Name: "extra_init_container", Image: "alpine:latest"},
				},
				ExtraVolumes: []corev1.Volume{
					{Name: "extra", VolumeSource: corev1.VolumeSource{EmptyDir: &corev1.EmptyDirVolumeSource{}}},
				},
			},
		},
		ManagedContainers: []v2alpha2.ManagedContainerSpec{
			{
				Name: "dummy-container",
				Env: []corev1.EnvVar{
					{Name: "key", Value: "value"},
					{Name: "ENV", Value: "env_new_value"},
					{Name: "ENV", Value: "env_new_value_2"},
					{Name: "ENV", Value: "env_new_value_3"},
				},
				Image: "alpine",
			},
		},
	}

	It("should patch workload spec", func() {
		podTemplate := newPodTemplateSpec()

		err := ApplyWorkloadSpec(workloadspec, podTemplate)
		Expect(err).ToNot(HaveOccurred())

		Expect(podTemplate.Spec.ImagePullSecrets).To(HaveLen(1))
		Expect(podTemplate.Labels).To(HaveKeyWithValue("custom-label", "custom-value"))
		Expect(podTemplate.Annotations).To(HaveKeyWithValue("custom-annotation", "custom-value"))
		Expect(podTemplate.Spec.InitContainers).To(HaveLen(1))
		Expect(podTemplate.Spec.Containers).To(HaveLen(2))

		marshaled, err := yaml.Marshal(podTemplate)
		Expect(err).NotTo(HaveOccurred())

		podTemplateYaml := string(marshaled)
		Expect(podTemplateYaml).To(ContainSubstring("extra_container"))
		Expect(podTemplateYaml).To(ContainSubstring("extra_init_container"))
		// strategic patching, override the old env var value.
		Expect(podTemplateYaml).To(ContainSubstring("env_new_value_3"))
		Expect(podTemplateYaml).ToNot(ContainSubstring("env_old_value"))
		GinkgoWriter.Println(podTemplateYaml)
	})

	It("should handle empty workload spec", func() {
		emptySpec := &v2alpha2.WorkloadSpec{}
		podTemplate := newPodTemplateSpec()

		err := ApplyWorkloadSpec(emptySpec, podTemplate)
		Expect(err).ToNot(HaveOccurred())

		Expect(equality.Semantic.DeepEqual(podTemplate, newPodTemplateSpec())).To(BeTrue())
	})

	It("should handle nil workload spec", func() {
		podTemplate := newPodTemplateSpec()

		err := ApplyWorkloadSpec(nil, podTemplate)
		Expect(err).ToNot(HaveOccurred())

		Expect(equality.Semantic.DeepEqual(podTemplate, newPodTemplateSpec())).To(BeTrue())
	})
})

func newPodTemplateSpec() *corev1.PodTemplateSpec {
	return &corev1.PodTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			Labels: map[string]string{
				"app": "dummy-app",
			},
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:  "dummy-container",
					Image: "alpine:latest",
					Env: []corev1.EnvVar{
						{Name: "ENV", Value: "env_old_value"},
					},
				},
			},
		},
	}
}
