package controllerutils

import (
	"fmt"

	corev1 "k8s.io/api/core/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/apiutils"
)

// DereferenceServiceProvider returns the service endpoint for the given service
// provider. It will dereference the Service if necessary.
func DereferenceServiceProvider(rtCtx *framework.RuntimeContext, provider v2alpha2.ServiceEndpoint, endpointType apiutils.ServiceEndpointType) (apiutils.ServiceEndpoint, error) {
	return apiutils.ResolveServiceProvider(rtCtx, provider, endpointType,
		apiutils.WithObjectDereferencer(dereferenceService))
}

func dereferenceService(rtCtx *framework.RuntimeContext, ref corev1.ObjectReference) (*corev1.Service, error) {
	// NOTE: Currently, we only support Service references. So we force it.
	ref.APIVersion = coreAPIVersion
	ref.Kind = kindService

	obj, err := Dereference(rtCtx, ref)
	if err != nil {
		return nil, fmt.Errorf("failed to dereference %s[%s/%s]: %w",
			ref.GroupVersionKind(), ref.Namespace, ref.Name, err)
	}

	svc, isService := obj.(*corev1.Service)
	if !isService {
		return nil, fmt.Errorf("object is not a Service: %T", obj)
	}

	return svc, nil
}
