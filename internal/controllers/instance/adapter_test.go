package instance

import (
	"context"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

var _ = Describe("Adapter", Ordered, func() {
	Describe("NewAdapter", func() {
		var (
			decoder    runtime.Decoder
			fakeScheme *runtime.Scheme
			fakeClient client.WithWatch
			instance   *v2alpha2.Instance
			manifest   []client.Object
			rtCtx      *framework.RuntimeContext
		)

		BeforeAll(func() {
			fakeScheme = runtime.NewScheme()
			utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
			utilruntime.Must(v2alpha2.AddToScheme(fakeScheme))

			decoder = testing.NewDecoder(fakeScheme)
		})

		BeforeEach(func() {
			framework.Scheme = fakeScheme

			rtCtx, _ = framework.NewRuntimeContext(context.Background())
		})

		AfterEach(func() {
			framework.Scheme = nil
			framework.Client = nil
		})

		JustBeforeEach(func() {
			fakeClient = fake.NewClientBuilder().
				WithScheme(fakeScheme).
				WithObjects(manifest...).
				WithStatusSubresource(manifest...).
				Build()

			framework.Client = fakeClient

			instance = selectObject[*v2alpha2.Instance](manifest, func(o client.Object) bool {
				_, ok := o.(*v2alpha2.Instance)

				return ok
			})

			Expect(instance).NotTo(BeNil())
		})

		Context("fresh install", func() {
			BeforeEach(func() {
				manifest = testing.ReadObjects("testdata/adapter/fresh-install.yaml", decoder)
			})

			It("should select the associated resources and deduce relevant information", func() {
				adapter, err := NewAdapter(rtCtx, instance)

				Expect(err).NotTo(HaveOccurred())

				Expect(adapter.Name).To(Equal("test"))
				Expect(adapter.Namespace).To(Equal("default"))
				Expect(adapter.Primary).NotTo(BeNil())
				Expect(adapter.Primary.Name).To(Equal("test"))
				Expect(adapter.Primary.Namespace).To(Equal("default"))
				Expect(adapter.ApplicationConfigName).To(Equal("test"))
				Expect(adapter.ApplicationServers).To(HaveLen(1))
				Expect(adapter.JobProcessors).To(HaveLen(1))

				err = adapter.Verify()

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.ResourceCheckDelay)))
			})
		})

		Context("invalid Instance setup", func() {
			When("ApplicationServer is not associated to the Instance", func() {
				BeforeEach(func() {
					manifest = testing.ReadObjects("testdata/adapter/unassociated-applicationserver.yaml", decoder)
				})

				It("should fail", func() {
					_, err := NewAdapter(rtCtx, instance)

					Expect(err).To(MatchError("instance does not have any ApplicationServers"))
				})
			})

			When("JobProcessor is not associated to the Instance", func() {
				BeforeEach(func() {
					manifest = testing.ReadObjects("testdata/adapter/unassociated-jobprocessor.yaml", decoder)
				})

				It("should fail", func() {
					_, err := NewAdapter(rtCtx, instance)

					Expect(err).To(MatchError("instance does not have any JobProcessors"))
				})
			})

			When("versions of ApplicationServers and JobProcessors are not the same", func() {
				BeforeEach(func() {
					manifest = testing.ReadObjects("testdata/adapter/inconsistent-versions.yaml", decoder)
				})

				It("should fail", func() {
					adapter, err := NewAdapter(rtCtx, instance)

					Expect(err).NotTo(HaveOccurred())

					err = adapter.Verify()

					Expect(err).To(MatchError(ContainSubstring("inconsistent JobProcessor version")))
				})
			})
		})

		Context("simple Instance setup", func() {
			BeforeEach(func() {
				manifest = testing.ReadObjects("testdata/adapter/simple-manifest.yaml", decoder)
			})

			It("should select the associated resources and deduce relevant information", func() {
				adapter, err := NewAdapter(rtCtx, instance)

				Expect(err).NotTo(HaveOccurred())

				Expect(adapter.Name).To(Equal("test"))
				Expect(adapter.Namespace).To(Equal("default"))
				Expect(adapter.Primary).NotTo(BeNil())
				Expect(adapter.Primary.Name).To(Equal("test"))
				Expect(adapter.Primary.Namespace).To(Equal("default"))
				Expect(adapter.ApplicationConfigName).To(Equal("test"))
				Expect(adapter.ApplicationServers).To(HaveLen(1))
				Expect(adapter.JobProcessors).To(HaveLen(1))

				err = adapter.Verify()

				Expect(err).NotTo(HaveOccurred())

				Expect(adapter.CurrentVersion).To(Equal("17.0.0"))
				Expect(adapter.CurrentEdition).To(Equal(v2alpha2.EE))
			})
		})

		Context("multi application Instance setup", func() {
			BeforeEach(func() {
				manifest = testing.ReadObjects("testdata/adapter/multi-app-manifest.yaml", decoder)
			})

			It("should select the associated resources and deduce relevant information", func() {
				adapter, err := NewAdapter(rtCtx, instance)

				Expect(err).NotTo(HaveOccurred())

				Expect(adapter.Name).To(Equal("test"))
				Expect(adapter.Namespace).To(Equal("default"))
				Expect(adapter.Primary).NotTo(BeNil())
				Expect(adapter.Primary.Name).To(Equal("test"))
				Expect(adapter.Primary.Namespace).To(Equal("default"))
				Expect(adapter.ApplicationConfigName).To(Equal("test"))
				Expect(adapter.ApplicationServers).To(HaveLen(2))
				Expect(adapter.JobProcessors).To(HaveLen(2))

				err = adapter.Verify()

				Expect(err).NotTo(HaveOccurred())

				Expect(adapter.CurrentVersion).To(Equal("17.0.0"))
				Expect(adapter.CurrentEdition).To(Equal(v2alpha2.EE))
			})
		})
	})

	DescribeTable("NeedsUpgrade",
		func(currentVersion, desiredVersion string, currentEdition, desiredEdition v2alpha2.Edition, expected bool) {
			adapter := &Adapter{
				CurrentVersion: currentVersion,
				CurrentEdition: currentEdition,
				source: &v2alpha2.Instance{
					Spec: v2alpha2.InstanceSpec{
						Version: desiredVersion,
						Edition: desiredEdition,
					},
				},
			}

			Expect(adapter.NeedsUpgrade()).To(Equal(expected))
		},
		Entry("different versions, unassigned desired edition",
			"17.0.0", "17.1.0", v2alpha2.EE, v2alpha2.Edition(""), true),
		Entry("same version, unassigned desired edition",
			"17.0.0", "17.0.0", v2alpha2.EE, v2alpha2.Edition(""), false),
		Entry("same version, same edition",
			"17.0.0", "17.0.0", v2alpha2.EE, v2alpha2.EE, false),
		Entry("same version, different edition",
			"17.0.0", "17.0.0", v2alpha2.EE, v2alpha2.CE, true),
		Entry("different version, same edition",
			"17.0.0", "17.1.0", v2alpha2.EE, v2alpha2.EE, true),
		Entry("different version, different edition",
			"17.0.0", "17.1.0", v2alpha2.CE, v2alpha2.EE, true))
})

func selectObjects[T client.Object](list []client.Object, criteria func(client.Object) bool) []T {
	result := []T{}

	for _, obj := range list {
		if criteria(obj) {
			t, isT := obj.(T)
			if isT {
				result = append(result, t)
			}
		}
	}

	return result
}

func selectObject[T client.Object](list []client.Object, criteria func(client.Object) bool) T {
	all := selectObjects[T](list, criteria)

	if len(all) == 0 {
		return utils.Zero[T]()
	}

	return all[0]
}
