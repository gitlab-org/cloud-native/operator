package instance

import (
	"errors"
	"fmt"
	"log/slog"

	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

const (
	instanceMarker      = "gitlab.io/instance"
	primaryMarker       = "gitlab.io/primary"
	migrationKindMarker = "gitlab.io/migration-kind"
)

// Adapter is a resource adapter for the Instance controller. It populates and
// validates the most important details that the Instance controller needs to
// function, including:
//
//   - Current and desired version and edition
//   - Associated workloads, namely ApplicationServers and JobProcessors
//   - Primary ApplicationServer that is used for deducing details such as the
//     ApplicationConfig that should be used for running DatabaseMigrations.
//
// Use NewAdapter to create a new Adapter from and Instance resource. When
// created successfully, use its Verify function to validate the details of the
// Adapter and detect the current version of the instance.
type Adapter struct {
	Name                  string
	Namespace             string
	CurrentVersion        string
	CurrentEdition        v2alpha2.Edition
	Primary               *v2alpha2.ApplicationServer
	ApplicationConfigName string
	ApplicationServers    []*v2alpha2.ApplicationServer
	JobProcessors         []*v2alpha2.JobProcessor

	logger slog.Logger
	source *v2alpha2.Instance
}

func NewAdapter(rtCtx *framework.RuntimeContext, source *v2alpha2.Instance) (*Adapter, error) {
	i := &Adapter{
		source: source,
		logger: *rtCtx.Logger.With("instance", client.ObjectKeyFromObject(source)),

		Name:      source.Name,
		Namespace: source.Namespace,
	}

	if err := i.populateApplicationServers(rtCtx); err != nil {
		return nil, fmt.Errorf("failed to populate ApplicationServers: %w", err)
	}

	if err := i.populateJobProcessors(rtCtx); err != nil {
		return nil, fmt.Errorf("failed to populate JobProcessors: %w", err)
	}

	if err := i.validate(); err != nil {
		return nil, err
	}

	if err := i.locateApplicationConfig(); err != nil {
		return nil, err
	}

	return i, nil
}

func (i *Adapter) Verify() error {
	if !i.canDetectVersionInfo() {
		return framework.RequeueWithDelay(Settings.Rails.ResourceCheckDelay)
	}

	return i.detectVersionInfo()
}

func (i *Adapter) NeedsUpgrade() bool {
	desiredVersion := i.source.Spec.Version
	desiredEdition := getEdition(i.source.Spec.Edition)

	return desiredVersion != i.CurrentVersion || desiredEdition != i.CurrentEdition
}

func (i *Adapter) populateApplicationServers(rtCtx *framework.RuntimeContext) error {
	i.logger.Debug("listing ApplicationServers")

	i.Primary = nil
	i.ApplicationServers = []*v2alpha2.ApplicationServer{}

	list := &v2alpha2.ApplicationServerList{}

	if err := framework.Client.List(rtCtx, list, &client.ListOptions{Namespace: i.source.Namespace}); err != nil {
		return err
	}

	for _, x := range list.Items {
		if x.Labels[instanceMarker] == i.source.Name {
			item := &x

			if item.Labels[primaryMarker] == "true" {
				i.Primary = item
			}

			i.ApplicationServers = append(i.ApplicationServers, item)
		}
	}

	i.logger.Debug("found ApplicationServers", "count", len(i.ApplicationServers))

	if i.Primary == nil && len(i.ApplicationServers) == 1 {
		i.Primary = i.ApplicationServers[0]

		i.logger.Debug("using the primary ApplicationServer", "reference", client.ObjectKeyFromObject(i.Primary))
	}

	return nil
}

func (i *Adapter) populateJobProcessors(rtCtx *framework.RuntimeContext) error {
	i.logger.Debug("listing JobProcessors")

	i.JobProcessors = []*v2alpha2.JobProcessor{}

	list := &v2alpha2.JobProcessorList{}

	if err := framework.Client.List(rtCtx, list, &client.ListOptions{Namespace: i.source.Namespace}); err != nil {
		return err
	}

	for _, x := range list.Items {
		if x.Labels[instanceMarker] == i.source.Name {
			item := &x

			i.JobProcessors = append(i.JobProcessors, item)
		}
	}

	i.logger.Debug("found JobProcessors", "count", len(i.JobProcessors))

	return nil
}

// To attempt zero-downtime upgrade process, the instance must have both
// ApplicationServers and JobProcessors.
func (i *Adapter) validate() error {
	if len(i.ApplicationServers) == 0 {
		return errors.New("instance does not have any ApplicationServers")
	}

	if len(i.JobProcessors) == 0 {
		return errors.New("instance does not have any JobProcessors")
	}

	if i.Primary == nil {
		return errors.New("instance does not have a primary ApplicationServer")
	}

	return nil
}

// ApplicationServer and JobProcessors of freshly installed instances do not
// have version details in their status sub-resource. Controller must wait for
// them to be ready before it can detect the version details.
func (i *Adapter) canDetectVersionInfo() bool {
	for _, appServer := range i.ApplicationServers {
		if appServer.Status.Version == "" {
			i.logger.Info("can not detect version details, ApplicationServer is not ready: %s/%s",
				appServer.Namespace, appServer.Name)

			return false
		}
	}

	for _, jobProcessor := range i.JobProcessors {
		if jobProcessor.Status.Version == "" {
			i.logger.Info("can not detect version details, JobProcessor is not ready: %s/%s",
				jobProcessor.Namespace, jobProcessor.Name)

			return false
		}
	}

	return true
}

func (i *Adapter) detectVersionInfo() error {
	if i.source.Status.Version == "" {
		// This is a new instance, possibly a new install or adoption. Use the
		// primary to determine the version details.
		i.logger.Info("a new instance has been created, detecting version details")

		i.CurrentVersion = i.Primary.Status.Version
		i.CurrentEdition = getEdition(i.Primary.Status.Edition)

		// Making sure that all ApplicationServers and JobProcessors have the same
		// version details. We don't support version skews.
		if err := i.validateVersionInfo(); err != nil {
			return err
		}
	} else {
		i.CurrentVersion = i.source.Status.Version
		i.CurrentEdition = getEdition(i.source.Status.Edition)
	}

	i.logger.Debug("detected current version details", "version", i.CurrentVersion, "edition", i.CurrentEdition)

	return nil
}

func (i *Adapter) validateVersionInfo() error {
	for _, appServer := range i.ApplicationServers {
		if appServer.Status.Version != i.CurrentVersion {
			return fmt.Errorf("inconsistent ApplicationServer version: %s/%s has %q, expected %q",
				appServer.Namespace, appServer.Name, appServer.Status.Version, i.CurrentVersion)
		}

		edition := getEdition(appServer.Status.Edition)
		if edition != i.CurrentEdition {
			return fmt.Errorf("inconsistent ApplicationServer edition: %s/%s has %s, expected %s",
				appServer.Namespace, appServer.Name, edition, i.CurrentEdition)
		}
	}

	for _, jobProcessor := range i.JobProcessors {
		if jobProcessor.Status.Version != i.CurrentVersion {
			return fmt.Errorf("inconsistent JobProcessor version: %s/%s has %q, expected %q",
				jobProcessor.Namespace, jobProcessor.Name, jobProcessor.Status.Version, i.CurrentVersion)
		}

		edition := getEdition(jobProcessor.Status.Edition)
		if edition != i.CurrentEdition {
			return fmt.Errorf("inconsistent JobProcessor edition: %s/%s has %s, expected %s",
				jobProcessor.Namespace, jobProcessor.Name, edition, i.CurrentEdition)
		}
	}

	return nil
}

// Find the ApplicationConfig reference. We need to to run database migrations.
func (i *Adapter) locateApplicationConfig() error {
	ref := i.Primary.Spec.ApplicationConfigRef

	i.logger.Debug("using ApplicationConfig",
		"apiVersion", ref.APIVersion,
		"kind", ref.Kind,
		"name", ref.Name,
		"namespace", ref.Namespace)

	if ref.Kind != "" && ref.Kind != "ApplicationConfig" {
		return fmt.Errorf("invalid ApplicationConfig kind: %s", ref.Kind)
	}

	if ref.APIVersion != "" && ref.APIVersion != v2alpha2.GroupVersion.String() {
		return fmt.Errorf("invalid ApplicationConfig apiVersion: %s", ref.APIVersion)
	}

	if ref.Namespace != "" && ref.Namespace != i.source.Namespace {
		return fmt.Errorf("invalid ApplicationConfig namespace: %s", ref.Namespace)
	}

	i.ApplicationConfigName = ref.Name

	return nil
}

func getEdition(e v2alpha2.Edition) v2alpha2.Edition {
	if e == "" {
		return defaultEdition
	}

	return e
}

var (
	defaultEdition = v2alpha2.Edition(settings.Get().Rails.Edition)
)
