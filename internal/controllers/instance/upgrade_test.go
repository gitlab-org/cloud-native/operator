package instance

import (
	"context"
	"fmt"
	"strings"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
)

var _ = Describe("ZeroDowntimeUpgrade", Ordered, func() {
	var (
		decoder         runtime.Decoder
		fakeScheme      *runtime.Scheme
		fakeClient      client.WithWatch
		instance        *v2alpha2.Instance
		manifest        []client.Object
		rtCtx           *framework.RuntimeContext
		preMigrationKey types.NamespacedName
		allMigrationKey types.NamespacedName
		adapter         *Adapter
		handler         UpgradeHandler
	)

	BeforeAll(func() {
		fakeScheme = runtime.NewScheme()
		utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
		utilruntime.Must(v2alpha2.AddToScheme(fakeScheme))

		decoder = testing.NewDecoder(fakeScheme)
	})

	BeforeEach(func() {
		framework.Scheme = fakeScheme

		rtCtx, _ = framework.NewRuntimeContext(context.Background())
	})

	AfterEach(func() {
		framework.Scheme = nil
		framework.Client = nil
	})

	JustBeforeEach(func() {
		fakeClient = fake.NewClientBuilder().
			WithScheme(fakeScheme).
			WithObjects(manifest...).
			WithStatusSubresource(manifest...).
			Build()

		framework.Client = fakeClient

		instance = selectObject[*v2alpha2.Instance](manifest, func(o client.Object) bool {
			_, ok := o.(*v2alpha2.Instance)

			return ok
		})

		Expect(instance).NotTo(BeNil())

		prefix := fmt.Sprintf("%s-%s-%s",
			instance.Name,
			instance.Spec.Version,
			strings.ToLower(string(getEdition(instance.Spec.Edition))))

		preMigrationKey = types.NamespacedName{
			Name:      prefix + "-pre",
			Namespace: instance.Namespace,
		}
		allMigrationKey = types.NamespacedName{
			Name:      prefix,
			Namespace: instance.Namespace,
		}

		var err error
		adapter, err = NewAdapter(rtCtx, instance)
		Expect(err).NotTo(HaveOccurred())

		err = adapter.Verify()
		Expect(err).NotTo(HaveOccurred())

		handler = NewZeroDowntimeUpgrade(adapter, instance.Spec.Version, instance.Spec.Edition)
	})

	Context("new and ready Instance", func() {
		BeforeEach(func() {
			manifest = testing.ReadObjects("testdata/upgrade/new-ready-install.yaml", decoder)
		})

		It("pause workload, update version details, and start pre-migrations", func() {
			state, err := handler.Upgrade(rtCtx, UpgradeStateStart)

			Expect(state).To(BeEquivalentTo(UpgradeStatePreMigrationsStarted))

			Expect(adapter.ApplicationServers[0].Spec.Paused).To(BeTrue())
			Expect(adapter.ApplicationServers[0].Spec.Version).To(Equal(instance.Spec.Version))
			Expect(adapter.ApplicationServers[0].Spec.Edition).To(Equal(v2alpha2.EE))
			Expect(adapter.JobProcessors[0].Spec.Paused).To(BeTrue())
			Expect(adapter.JobProcessors[0].Spec.Version).To(Equal(instance.Spec.Version))
			Expect(adapter.JobProcessors[0].Spec.Edition).To(Equal(v2alpha2.EE))

			Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Migrations.StatusCheckDelay)))

			dbm := &v2alpha2.DatabaseMigration{}
			err = fakeClient.Get(rtCtx, preMigrationKey, dbm)

			Expect(err).NotTo(HaveOccurred())

			Expect(dbm.ObjectMeta.OwnerReferences).To(HaveLen(1))
			Expect(dbm.ObjectMeta.OwnerReferences[0].Kind).To(Equal("Instance"))
			Expect(dbm.ObjectMeta.OwnerReferences[0].Name).To(Equal(instance.Name))

			Expect(dbm.Spec.Version).To(Equal(instance.Spec.Version))
			Expect(dbm.Spec.Edition).To(Equal(getEdition(instance.Spec.Edition)))
			Expect(dbm.Spec.ApplicationConfigRef.Name).To(Equal(adapter.ApplicationConfigName))
			Expect(dbm.Spec.SkipPostMigrations).To(BeTrue())
			Expect(dbm.Spec.IsUpgrade).To(BeTrue())
		})
	})

	Context("pre-migrations started", func() {
		BeforeEach(func() {
			manifest = testing.ReadObjects("testdata/upgrade/pre-migrations-started.yaml", decoder)
		})

		When("pre-migrations is not finished", func() {
			It("waits for pre-migrations to complete", func() {
				state, err := handler.Upgrade(rtCtx, UpgradeStatePreMigrationsStarted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePreMigrationsStarted))

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Migrations.StatusCheckDelay)))
			})
		})

		When("pre-migrations completed successfully", func() {
			It("resumes workload, bypasses schema version check, and waits for workloads to be ready", func() {
				setDatabaseMigrationCondition(rtCtx, preMigrationKey, metav1.Condition{
					Type:    v2alpha2.DatabaseMigrationCompletedCondition,
					Status:  metav1.ConditionTrue,
					Reason:  v2alpha2.DatabaseMigrationSucceededReason,
					Message: "Test",
				})

				state, err := handler.Upgrade(rtCtx, UpgradeStatePreMigrationsStarted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePostMigrationsStarted))

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Migrations.StatusCheckDelay)))

				Expect(adapter.ApplicationServers[0].Spec.Paused).To(BeFalse())
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers).NotTo(BeEmpty())
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Name).To(Equal("dependencies"))
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Env[0].Name).To(Equal("BYPASS_SCHEMA_VERSION"))
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Env[0].Value).To(Equal("true"))
				Expect(adapter.JobProcessors[0].Spec.Paused).To(BeFalse())
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers).NotTo(BeEmpty())
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Name).To(Equal("dependencies"))
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Env[0].Name).To(Equal("BYPASS_SCHEMA_VERSION"))
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Env[0].Value).To(Equal("true"))
			})
		})

		When("pre-migrations failed", func() {
			It("fails upgrade and reverts version details", func() {
				setDatabaseMigrationCondition(rtCtx, preMigrationKey, metav1.Condition{
					Type:    v2alpha2.DatabaseMigrationCompletedCondition,
					Status:  metav1.ConditionFalse,
					Reason:  v2alpha2.DatabaseMigrationFailedReason,
					Message: "Test",
				})

				state, err := handler.Upgrade(rtCtx, UpgradeStatePreMigrationsStarted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePreMigrationsStarted))

				Expect(framework.IsTerminal(err)).To(BeTrue())
				Expect(err).To(MatchError(ContainSubstring("database migration failed, upgrade cannot proceed")))

				Expect(adapter.ApplicationServers[0].Spec.Version).To(Equal(adapter.CurrentVersion))
				Expect(adapter.ApplicationServers[0].Spec.Edition).To(Equal(adapter.CurrentEdition))
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers).To(BeEmpty())
				Expect(adapter.JobProcessors[0].Spec.Version).To(Equal(adapter.CurrentVersion))
				Expect(adapter.JobProcessors[0].Spec.Edition).To(Equal(adapter.CurrentEdition))
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers).To(BeEmpty())
			})
		})
	})

	Context("pre-migrations completed", func() {
		BeforeEach(func() {
			manifest = testing.ReadObjects("testdata/upgrade/pre-migrations-completed.yaml", decoder)
		})

		When("workload is not ready", func() {
			It("waits for workload to be ready", func() {
				state, err := handler.Upgrade(rtCtx, UpgradeStatePreMigrationsCompleted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePreMigrationsCompleted))

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Puma.StatusCheckDelay)))
			})
		})

		When("workload is ready", func() {
			It("doesn't change workload spec and starts the full database migrations", func() {
				setApplicationServerCondition(rtCtx, client.ObjectKeyFromObject(adapter.ApplicationServers[0]), metav1.Condition{
					Type:    v2alpha2.ApplicationServerReadyCondition,
					Status:  metav1.ConditionTrue,
					Reason:  v2alpha2.WorkloadAvailableReason,
					Message: "Test",
				})
				setJobProcessorCondition(rtCtx, client.ObjectKeyFromObject(adapter.JobProcessors[0]), metav1.Condition{
					Type:    v2alpha2.JobProcessorReadyCondition,
					Status:  metav1.ConditionTrue,
					Reason:  v2alpha2.WorkloadAvailableReason,
					Message: "Test",
				})

				state, err := handler.Upgrade(rtCtx, UpgradeStatePreMigrationsCompleted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePostMigrationsStarted))

				Expect(adapter.ApplicationServers[0].Spec.Paused).To(BeFalse())
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers).NotTo(BeEmpty())
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Name).To(Equal("dependencies"))
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Env[0].Name).To(Equal("BYPASS_SCHEMA_VERSION"))
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Env[0].Value).To(Equal("true"))
				Expect(adapter.JobProcessors[0].Spec.Paused).To(BeFalse())
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers).NotTo(BeEmpty())
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Name).To(Equal("dependencies"))
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Env[0].Name).To(Equal("BYPASS_SCHEMA_VERSION"))
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Env[0].Value).To(Equal("true"))

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Migrations.StatusCheckDelay)))

				dbm := &v2alpha2.DatabaseMigration{}
				err = fakeClient.Get(rtCtx, allMigrationKey, dbm)

				Expect(err).NotTo(HaveOccurred())

				Expect(dbm.ObjectMeta.OwnerReferences).To(HaveLen(1))
				Expect(dbm.ObjectMeta.OwnerReferences[0].Kind).To(Equal("Instance"))
				Expect(dbm.ObjectMeta.OwnerReferences[0].Name).To(Equal(instance.Name))

				Expect(dbm.Spec.Version).To(Equal(instance.Spec.Version))
				Expect(dbm.Spec.Edition).To(Equal(getEdition(instance.Spec.Edition)))
				Expect(dbm.Spec.ApplicationConfigRef.Name).To(Equal(adapter.ApplicationConfigName))
				Expect(dbm.Spec.SkipPostMigrations).To(BeFalse())
				Expect(dbm.Spec.IsUpgrade).To(BeTrue())
			})
		})

		When("workload failed", func() {
			It("waits for workload to be ready", func() {
				setApplicationServerCondition(rtCtx, client.ObjectKeyFromObject(adapter.ApplicationServers[0]), metav1.Condition{
					Type:    v2alpha2.ApplicationServerReadyCondition,
					Status:  metav1.ConditionFalse,
					Reason:  v2alpha2.WorkloadFailedReason,
					Message: "Test",
				})

				state, err := handler.Upgrade(rtCtx, UpgradeStatePreMigrationsCompleted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePreMigrationsCompleted))

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Puma.StatusCheckDelay)))
			})
		})
	})

	Context("full migrations started", func() {
		BeforeEach(func() {
			manifest = testing.ReadObjects("testdata/upgrade/full-migrations-started.yaml", decoder)
		})

		When("migrations are not finished", func() {
			It("assumes workloads are resumed and version details are updated, and waits for migrations to complete", func() {
				state, err := handler.Upgrade(rtCtx, UpgradeStatePostMigrationsStarted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePostMigrationsStarted))

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Migrations.StatusCheckDelay)))
			})
		})

		When("migrations completed successfully", func() {
			It("waits for workloads to be ready", func() {
				setDatabaseMigrationCondition(rtCtx, allMigrationKey, metav1.Condition{
					Type:    v2alpha2.DatabaseMigrationCompletedCondition,
					Status:  metav1.ConditionTrue,
					Reason:  v2alpha2.DatabaseMigrationSucceededReason,
					Message: "Test",
				})

				state, err := handler.Upgrade(rtCtx, UpgradeStatePostMigrationsStarted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePostMigrationsCompleted))

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Puma.StatusCheckDelay)))
			})
		})

		When("migrations failed", func() {
			It("fails upgrade and doesn't make any change to workload spec", func() {
				setDatabaseMigrationCondition(rtCtx, allMigrationKey, metav1.Condition{
					Type:    v2alpha2.DatabaseMigrationCompletedCondition,
					Status:  metav1.ConditionFalse,
					Reason:  v2alpha2.DatabaseMigrationFailedReason,
					Message: "Test",
				})

				state, err := handler.Upgrade(rtCtx, UpgradeStatePostMigrationsStarted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePostMigrationsStarted))

				Expect(framework.IsTerminal(err)).To(BeTrue())
				Expect(err).To(MatchError(ContainSubstring("database migration failed")))

				Expect(adapter.ApplicationServers[0].Spec.Paused).To(BeFalse())
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers).NotTo(BeEmpty())
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Name).To(Equal("dependencies"))
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Env[0].Name).To(Equal("BYPASS_SCHEMA_VERSION"))
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Env[0].Value).To(Equal("true"))
				Expect(adapter.JobProcessors[0].Spec.Paused).To(BeFalse())
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers).NotTo(BeEmpty())
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Name).To(Equal("dependencies"))
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Env[0].Name).To(Equal("BYPASS_SCHEMA_VERSION"))
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Env[0].Value).To(Equal("true"))
			})
		})
	})

	Context("full migrations completed", func() {
		BeforeEach(func() {
			manifest = testing.ReadObjects("testdata/upgrade/full-migrations-completed.yaml", decoder)
		})

		When("workload is not ready", func() {
			It("waits for workload to be ready", func() {
				state, err := handler.Upgrade(rtCtx, UpgradeStatePostMigrationsCompleted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePostMigrationsCompleted))

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Puma.StatusCheckDelay)))
			})
		})

		When("workload is ready", func() {
			It("finishes the upgrade procedure", func() {
				setApplicationServerCondition(rtCtx, client.ObjectKeyFromObject(adapter.ApplicationServers[0]), metav1.Condition{
					Type:    v2alpha2.ApplicationServerReadyCondition,
					Status:  metav1.ConditionTrue,
					Reason:  v2alpha2.WorkloadAvailableReason,
					Message: "Test",
				})
				setJobProcessorCondition(rtCtx, client.ObjectKeyFromObject(adapter.JobProcessors[0]), metav1.Condition{
					Type:    v2alpha2.JobProcessorReadyCondition,
					Status:  metav1.ConditionTrue,
					Reason:  v2alpha2.WorkloadAvailableReason,
					Message: "Test",
				})

				state, err := handler.Upgrade(rtCtx, UpgradeStatePostMigrationsCompleted)

				// NOTE: We cannot test the state transition to `FinalizingNewVersion`
				//       with this fixture. In practice, `finalizeNewVersion`,
				//       waits for workload to be ready, then enables schema
				//       checking. This will trigger the workload controllers
				//       and the workload will be unavailable. This test fixture
				//       cannot easily imitate this behavior.

				Expect(state).To(BeEquivalentTo(UpgradeStateCompleted))
				Expect(err).NotTo(HaveOccurred())

				Expect(adapter.ApplicationServers[0].Spec.Paused).To(BeFalse())
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers).NotTo(BeEmpty())
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Name).To(Equal("dependencies"))
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Env[0].Name).To(Equal("BYPASS_SCHEMA_VERSION"))
				Expect(adapter.ApplicationServers[0].Spec.Workload.ManagedContainers[0].Env[0].Value).To(Equal("false"))
				Expect(adapter.JobProcessors[0].Spec.Paused).To(BeFalse())
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers).NotTo(BeEmpty())
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Name).To(Equal("dependencies"))
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Env[0].Name).To(Equal("BYPASS_SCHEMA_VERSION"))
				Expect(adapter.JobProcessors[0].Spec.Workload.ManagedContainers[0].Env[0].Value).To(Equal("false"))
			})
		})

		When("workload is failed", func() {
			It("waits for workload to be ready", func() {
				setApplicationServerCondition(rtCtx, client.ObjectKeyFromObject(adapter.ApplicationServers[0]), metav1.Condition{
					Type:    v2alpha2.ApplicationServerReadyCondition,
					Status:  metav1.ConditionFalse,
					Reason:  v2alpha2.WorkloadFailedReason,
					Message: "Test",
				})

				state, err := handler.Upgrade(rtCtx, UpgradeStatePostMigrationsCompleted)

				Expect(state).To(BeEquivalentTo(UpgradeStatePostMigrationsCompleted))

				Expect(err).To(MatchError(framework.RequeueWithDelay(Settings.Rails.Puma.StatusCheckDelay)))
			})
		})
	})
})

func setDatabaseMigrationCondition(ctx context.Context, key types.NamespacedName, condition metav1.Condition) {
	dbm := &v2alpha2.DatabaseMigration{}

	err := framework.Client.Get(ctx, key, dbm)
	Expect(err).NotTo(HaveOccurred())

	meta.SetStatusCondition(&dbm.Status.Conditions, condition)

	err = framework.Client.Status().Update(ctx, dbm)
	Expect(err).NotTo(HaveOccurred())
}

func setApplicationServerCondition(ctx context.Context, key types.NamespacedName, condition metav1.Condition) {
	appServer := &v2alpha2.ApplicationServer{}

	err := framework.Client.Get(ctx, key, appServer)
	Expect(err).NotTo(HaveOccurred())

	meta.SetStatusCondition(&appServer.Status.Conditions, condition)

	err = framework.Client.Status().Update(ctx, appServer)
	Expect(err).NotTo(HaveOccurred())
}

func setJobProcessorCondition(ctx context.Context, key types.NamespacedName, condition metav1.Condition) {
	jobProcessor := &v2alpha2.JobProcessor{}

	err := framework.Client.Get(ctx, key, jobProcessor)
	Expect(err).NotTo(HaveOccurred())

	meta.SetStatusCondition(&jobProcessor.Status.Conditions, condition)

	err = framework.Client.Status().Update(ctx, jobProcessor)
	Expect(err).NotTo(HaveOccurred())
}
