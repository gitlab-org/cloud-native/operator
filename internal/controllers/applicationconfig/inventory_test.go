package applicationconfig

import (
	"context"

	"gopkg.in/yaml.v3"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"sigs.k8s.io/controller-runtime/pkg/client/fake"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework/inventory"

	"gitlab.com/gitlab-org/cloud-native/operator/internal/settings"
)

var _ = Describe("RailsSecretsGenerator", Ordered, func() {
	var (
		appConfig   *v2alpha2.ApplicationConfig
		railsSecret *corev1.Secret
		rtCtx       *framework.RuntimeContext
	)

	BeforeAll(func() {
		appConfig = &v2alpha2.ApplicationConfig{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test",
				Namespace: "default",
			},
		}

		fakeScheme := runtime.NewScheme()
		utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
		utilruntime.Must(v2alpha2.AddToScheme(fakeScheme))

		fakeClient := fake.NewClientBuilder().
			WithScheme(fakeScheme).
			WithObjects(appConfig).
			Build()

		framework.Scheme = fakeScheme
		framework.Client = fakeClient

		rtCtx, _ = framework.NewRuntimeContext(context.Background())

		railsSecretsInventory := inventory.Decorate[*corev1.Secret](railsSecretsGenerator,
			inventory.WithNamespace(appConfig.Namespace),
			inventory.WithName(appConfig.Name+railsSecretNameSuffix),
		)

		secrets, err := railsSecretsInventory.Get(rtCtx)

		Expect(err).NotTo(HaveOccurred())
		Expect(len(secrets)).To(Equal(1))

		railsSecret = secrets[0]
	})

	AfterEach(func() {
		framework.Scheme = nil
		framework.Client = nil
	})

	It("has the correct basic configuration", func() {
		Expect(railsSecret.Name).To(Equal("test-rails-secrets"))
		Expect(railsSecret.Namespace).To(Equal("default"))
		Expect(railsSecret.Type).To(Equal(corev1.SecretTypeOpaque))
	})

	It("has the correct Rails secrets", func() {
		railsEnvName := settings.Get().Rails.Environment
		var railsSecretData map[string]any

		err := yaml.Unmarshal(railsSecret.Data[railsSecretYamlName], &railsSecretData)

		Expect(err).NotTo(HaveOccurred())
		Expect(railsSecretData).To(HaveKey(railsEnvName))

		railsSecrets := railsSecretData[railsEnvName].(map[string]any)

		// Check for RSA private key
		Expect(railsSecrets).To(HaveKeyWithValue(
			"openid_connect_signing_key",
			ContainSubstring("BEGIN PRIVATE KEY")))

		// Check for hexadecimal secrets (128 characters long)
		hexSecrets := []string{
			"secret_key_base",
			"otp_key_base",
			"db_key_base",
			"encrypted_settings_key_base",
		}
		for _, secretName := range hexSecrets {
			Expect(railsSecrets).To(HaveKeyWithValue(
				secretName,
				MatchRegexp("^[0-9a-f]{128}$")))
		}

		// Check for alphanumeric secrets (32 characters long)
		alphaNumScalarSecrets := []string{
			"active_record_encryption_key_derivation_salt",
		}

		for _, secretName := range alphaNumScalarSecrets {
			Expect(railsSecrets).To(HaveKeyWithValue(
				secretName,
				MatchRegexp("^[0-9a-zA-Z]{32}$")))
		}

		alphaNumVectorSecrets := []string{
			"active_record_encryption_primary_key",
			"active_record_encryption_deterministic_key",
		}

		for _, secretName := range alphaNumVectorSecrets {
			Expect(railsSecrets).To(HaveKeyWithValue(
				secretName,
				ConsistOf(MatchRegexp("^[0-9a-zA-Z]{32}$"))))
		}

	})
})
