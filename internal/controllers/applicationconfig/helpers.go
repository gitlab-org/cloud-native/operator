package applicationconfig

import (
	"gopkg.in/yaml.v3"

	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/secret"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/support/utils"
)

// Similar to Rails configuration, Rails secrets in `config/secrets.yml` are
// listed under an environment name, for example:
//
//	production:
//		secret_key_base:
//		otp_key_base:
//		db_key_base:
//		encrypted_settings_key_base:
//		openid_connect_signing_key:
//
// When GitLab Rails application can not find a secret for the environment, it
// silently generates a new value. This is very dangerous because the secrets
// that are used are not persisted.
//
// References:
//
//   - https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/secrets.yml.example
//   - https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/initializers/01_secret_token.rb
//
// This function decorates and formats the generated secrets in a way that GitLab
// Rails can understand.
//
// The new secret generator:
//   - Calls all the provided generators and collects their payloads.
//   - Formats the payloads into a YAML, uses the specified environment.
//   - Makes adjustments to final YAML where needed. This includes all sorts of
//     Rails weird stuff!
//   - Returns a new payload with a single key that contains the YAML.
func shapeRailsSecrets(generators []secret.Generator, name, environment string) secret.GeneratorFunc {
	return func() (secret.Payload, error) {
		aggregate := map[string]any{}

		for _, generator := range generators {
			payload, err := generator.Generate()
			if err != nil {
				return nil, err
			}

			aggregate = utils.MergeMap(aggregate, adjustPayload(toStringMap(payload)))
		}

		railsSecretsYAML, err := yaml.Marshal(map[string]any{
			environment: aggregate,
		})

		if err != nil {
			return nil, err
		}

		return secret.Payload{
			name: railsSecretsYAML,
		}, nil
	}
}

func toStringMap(p secret.Payload) map[string]any {
	result := map[string]any{}

	for k, v := range p.AsMap() {
		result[k] = string(v)
	}

	return result
}

func adjustPayload(p map[string]any) map[string]any {
	result := p

	// Put all your weird stuff here! We don't want to spill it all over the
	// place.
	//
	// Converting scalar values to arrays for ActiveRecord encryption keys
	// Why!? ¯\_(ツ)_/¯.
	//
	// See: https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/initializers/01_secret_token.rb#L78
	activeRecordEncKeys := []string{
		"active_record_encryption_primary_key",
		"active_record_encryption_deterministic_key",
	}

	for _, key := range activeRecordEncKeys {
		if v, ok := result[key]; ok {
			result[key] = []any{v}
		}
	}

	return result
}
