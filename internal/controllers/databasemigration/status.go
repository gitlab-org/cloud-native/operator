package databasemigration

import (
	"fmt"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/internal/pkg/controllerutils"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

func (c *Controller) checkReadCondition(ctx *framework.RuntimeContext) error {
	ctx.Logger.Debug("Checking application server readiness")

	job, err := c.getJob(ctx)
	if err != nil {
		return fmt.Errorf("failed to get Job: %w", err)
	}

	err = controllerutils.PatchStatus(ctx, c.dbMigration, func() bool {
		changed := c.dbMigration.Status.StartTime == nil && job.Status.StartTime != nil
		changed = changed || (c.dbMigration.Status.CompletionTime == nil && job.Status.CompletionTime != nil)

		c.dbMigration.Status.StartTime = job.Status.StartTime
		c.dbMigration.Status.CompletionTime = job.Status.CompletionTime

		return changed
	})
	if err != nil {
		ctx.Logger.Debug("Failed to update database migration status", "error", err)
		return fmt.Errorf("failed to update database migration status: %w", err)
	}

	jobCompleted := controllerutils.JobCompleted(job)

	err = controllerutils.WaitFor(ctx, jobCompleted, controllerutils.WithDelay(Settings.Rails.Migrations.StatusCheckDelay))
	if framework.IsTerminal(err) {
		ctx.Logger.Debug("Migration Job failed", "error", err)

		if err := c.setStatusCondition(ctx, metav1.Condition{
			Type:    v2alpha2.DatabaseMigrationCompletedCondition,
			Status:  metav1.ConditionFalse,
			Reason:  v2alpha2.DatabaseMigrationFailedReason,
			Message: "Database migration is failed: " + err.Error(),
		}); err != nil {
			ctx.Logger.Debug("Failed to set status condition", "error", err)
			return fmt.Errorf("failed to set status condition: %w", err)
		}

		return err
	} else if err != nil {
		ctx.Logger.Debug("Job is not ready",
			"Job.Status.Conditions", job.Status.Conditions)

		if err := c.setStatusCondition(ctx, metav1.Condition{
			Type:   v2alpha2.DatabaseMigrationCompletedCondition,
			Status: metav1.ConditionUnknown,
			Reason: v2alpha2.DatabaseMigrationRunningReason,
		}); err != nil {
			ctx.Logger.Debug("Failed to set status condition", "error", err)
			return fmt.Errorf("failed to set status condition: %w", err)
		}

		return err
	}

	err = c.setStatusCondition(ctx, metav1.Condition{
		Type:    v2alpha2.DatabaseMigrationCompletedCondition,
		Status:  metav1.ConditionTrue,
		Reason:  v2alpha2.DatabaseMigrationSucceededReason,
		Message: "Application workload is available",
	})
	if err != nil {
		ctx.Logger.Debug("Failed to set status condition", "error", err)
		return fmt.Errorf("failed to set status condition: %w", err)
	}

	return nil
}
