package applicationserver

import (
	corev1 "k8s.io/api/core/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	gitlabcomv2alpha2 "gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
)

func refFromObject(obj client.Object) corev1.ObjectReference {
	gvk := obj.GetObjectKind().GroupVersionKind()
	if gvk.Empty() {
		// try get gvk
		gvks, _, err := framework.Scheme.ObjectKinds(obj)
		if err == nil && len(gvks) > 0 {
			gvk = gvks[0]
		}
	}

	return corev1.ObjectReference{
		APIVersion:      gvk.GroupVersion().String(),
		Kind:            gvk.Kind,
		Namespace:       obj.GetNamespace(),
		Name:            obj.GetName(),
		UID:             obj.GetUID(),
		ResourceVersion: obj.GetResourceVersion(),
		FieldPath:       obj.GetSelfLink(),
	}
}

func sharedRefFromObject(obj client.Object, usage string) gitlabcomv2alpha2.SharedObjectReference {
	ref := refFromObject(obj)

	return gitlabcomv2alpha2.SharedObjectReference{
		Name:       ref.Name,
		Namespace:  ref.Namespace,
		APIVersion: ref.APIVersion,
		Kind:       ref.Kind,
		Usage:      usage,
	}
}
