package applicationserver

import (
	"log/slog"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	monitoringv1 "github.com/prometheus-operator/prometheus-operator/pkg/apis/monitoring/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"

	"gitlab.com/gitlab-org/cloud-native/operator/api/v2alpha2"
	"gitlab.com/gitlab-org/cloud-native/operator/pkg/framework"
	libtesting "gitlab.com/gitlab-org/cloud-native/operator/pkg/support/testing"
)

func TestApplicationServerController(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GitLab Operator: ApplicationServer Controller")
}

var _ = BeforeSuite(func() {
	slog.SetDefault(slog.New(&libtesting.GinkgoHandler{}))
	slog.SetLogLoggerLevel(slog.LevelDebug)

	fakeScheme := runtime.NewScheme()
	utilruntime.Must(clientgoscheme.AddToScheme(fakeScheme))
	utilruntime.Must(v2alpha2.AddToScheme(fakeScheme))
	utilruntime.Must(monitoringv1.AddToScheme(fakeScheme))

	framework.Scheme = fakeScheme
})

var _ = AfterSuite(func() {
	framework.Scheme = nil
	framework.Client = nil
})
